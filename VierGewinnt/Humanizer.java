import java.util.*;

/* A primitive name- and taunt- chooser for the primitive brute-force-games-set by Daniel Missal */

public class Humanizer
{
	private Random randomizer = new Random();

	private String alreadyProvidedPlayerName = "";
	private ArrayList<String> possiblePlayerName = new ArrayList<String>();
	private ArrayList<Integer> playerNameProbabilityLevelValue = new ArrayList<Integer>();
	private int playerNameProbabilityLevelSum = 0;

	private ArrayList<String> alreadyProvidedNegativeCritiqueTaunt = new ArrayList<String>();
	private ArrayList<String> possibleNegativeCritiqueTaunt = new ArrayList<String>();
	private ArrayList<Integer> negativeCritiqueTauntProbabilityLevelValue = new ArrayList<Integer>();
	private int negativeCritiqueTauntProbabilityLevelSum = 0;

	private ArrayList<String> alreadyProvidedPositiveCritiqueTaunt = new ArrayList<String>();
	private ArrayList<String> possiblePositiveCritiqueTaunt = new ArrayList<String>();
	private ArrayList<Integer> positiveCritiqueTauntProbabilityLevelValue = new ArrayList<Integer>();
	private int positiveCritiqueTauntProbabilityLevelSum = 0;

	private ArrayList<String> alreadyProvidedPositiveAnnouncementTaunt = new ArrayList<String>();
	private ArrayList<String> possiblePositiveAnnouncementTaunt = new ArrayList<String>();
	private ArrayList<Integer> positiveAnnouncementTauntProbabilityLevelValue = new ArrayList<Integer>();
	private int positiveAnnouncementTauntProbabilityLevelSum = 0;

	private ArrayList<String> alreadyProvidedNegativeAnnouncementTaunt = new ArrayList<String>();
	private ArrayList<String> possibleNegativeAnnouncementTaunt = new ArrayList<String>();
	private ArrayList<Integer> negativeAnnouncementTauntProbabilityLevelValue = new ArrayList<Integer>();
	private int negativeAnnouncementTauntProbabilityLevelSum = 0;

	public Humanizer()
	{
		addName("Henry",9);
		addName("Richard",9);
		addName("Josh",7);
		addName("Bob",9);
		addName("Chris",9);
		addName("Marc",5);
		addName("mok",2);
		addName("dam",2);
		addName("hru",2);
		addName("Matthew",5);
		addName("Dylan",3);
		addName("Joe",6);
		addName("King",2);
		addName("Dave",8);
		addName("Ashley",4);
		addName("Tamara",4);
		addName("Barbara",9);
		addName("Alice",8);
		addName("Sally",6);

		addNegativeCritiqueTaunt("Schlechter Zug...",1);
		addNegativeCritiqueTaunt("Das k�nnte ich besser.",1);
		addNegativeCritiqueTaunt("Langweilig.",1);
		addNegativeCritiqueTaunt("Denk dir mal 'was Neues aus...",1);
		addNegativeCritiqueTaunt("Na, das solltest du aber nochmal �ben..",1);

		addPositiveCritiqueTaunt("Okay, das reicht jetzt!",1);
		addPositiveCritiqueTaunt("Gar nicht schlecht f�r so ein kleines Hirn!",1);
		addPositiveCritiqueTaunt("Guter Zug.",1);
		addPositiveCritiqueTaunt("Hmm... ja, da muss ich meine Strategie nochmal �berdenken.",1);

		addPositiveAnnouncementTaunt("Sieh zu und lerne!",1);
		addPositiveAnnouncementTaunt("Mit dir hat man's ja leicht.",1);
		addPositiveAnnouncementTaunt("Pass auf: So macht man das.",1);
		addPositiveAnnouncementTaunt("Okay, lass mich mal das hier probieren:",1);

		addNegativeAnnouncementTaunt("Hmm... vielleicht h�tte ich lieber...",1);
		addNegativeAnnouncementTaunt("Das hast du mit Absicht gemacht!",1);
		addNegativeAnnouncementTaunt("Okay, jetzt sag mir endlich...",1);
		addNegativeAnnouncementTaunt("Okay, das ist vielleicht auch ein Fehler.",1);
		addNegativeAnnouncementTaunt("Ja, ich wei�, du h�ttest irgendetwas Besseres gemacht...",1);
	}

	public String providePlayerName()
	{
		String chosenPlayerName = "";

		while ((chosenPlayerName.equals(alreadyProvidedPlayerName)) || chosenPlayerName.equals(""))
		{
			int randomInt = randomizer.nextInt(playerNameProbabilityLevelSum);

			Iterator nameIterator = possiblePlayerName.iterator();
			Iterator levelValueIterator = playerNameProbabilityLevelValue.iterator();

			int i = 0;
			while (i < randomInt)
			{
				chosenPlayerName = (String) nameIterator.next();

				i += (int)(Integer) levelValueIterator.next();
			}
		}

		alreadyProvidedPlayerName = chosenPlayerName;

		return chosenPlayerName;
	}

	public String provideCritiqueTaunt(float evaluationOfPosition)
	{
		if (evaluationOfPosition > 0)
		{
			return provideNegativeCritiqueTaunt();
		}
		else
		{
			return providePositiveCritiqueTaunt();
		}
	}

	private	String provideNegativeCritiqueTaunt()
	{
		String chosenNegativeCritiqueTaunt = "";

		int numberOfAttempt = 1;

		while ((alreadyProvidedNegativeCritiqueTaunt.contains(chosenNegativeCritiqueTaunt)) || (chosenNegativeCritiqueTaunt.equals("")))
		{
			if (numberOfAttempt > 100)
			{
				break;
			}

			int randomInt = randomizer.nextInt(negativeCritiqueTauntProbabilityLevelSum);

			Iterator tauntIterator = possibleNegativeCritiqueTaunt.iterator();
			Iterator levelValueIterator = negativeCritiqueTauntProbabilityLevelValue.iterator();

			int i = 0;
			while (i < randomInt)
			{
				chosenNegativeCritiqueTaunt = (String) tauntIterator.next();

				i += (int)(Integer) levelValueIterator.next();
			}

			numberOfAttempt += 1;
		}

		alreadyProvidedNegativeCritiqueTaunt.add(chosenNegativeCritiqueTaunt);

		return chosenNegativeCritiqueTaunt;
	}

	private	String providePositiveCritiqueTaunt()
	{
		String chosenPositiveCritiqueTaunt = "";

		int numberOfAttempt = 1;

		while ((alreadyProvidedPositiveCritiqueTaunt.contains(chosenPositiveCritiqueTaunt)) || (chosenPositiveCritiqueTaunt.equals("")))
		{
			if (numberOfAttempt > 100)
			{
				break;
			}

			int randomInt = randomizer.nextInt(positiveCritiqueTauntProbabilityLevelSum);

			Iterator tauntIterator = possiblePositiveCritiqueTaunt.iterator();
			Iterator levelValueIterator = positiveCritiqueTauntProbabilityLevelValue.iterator();

			int i = 0;
			while (i < randomInt)
			{
				chosenPositiveCritiqueTaunt = (String) tauntIterator.next();

				i += (int)(Integer) levelValueIterator.next();
			}

			numberOfAttempt += 1;
		}

		alreadyProvidedPositiveCritiqueTaunt.add(chosenPositiveCritiqueTaunt);

		return chosenPositiveCritiqueTaunt;
	}

	public String provideAnnouncementTaunt(float evaluationOfPosition)
	{
		if (evaluationOfPosition > 0)
		{
			return providePositiveAnnouncementTaunt();
		}
		else
		{
			return provideNegativeAnnouncementTaunt();
		}
	}

	private	String providePositiveAnnouncementTaunt()
	{
		String chosenPositiveAnnouncementTaunt = "";

		int numberOfAttempt = 1;

		while ((alreadyProvidedPositiveAnnouncementTaunt.contains(chosenPositiveAnnouncementTaunt))
		       ||
		       (chosenPositiveAnnouncementTaunt.equals("")))
		{
			if (numberOfAttempt > 100)
			{
				break;
			}

			int randomInt = randomizer.nextInt(positiveAnnouncementTauntProbabilityLevelSum);

			Iterator tauntIterator = possiblePositiveAnnouncementTaunt.iterator();
			Iterator levelValueIterator = positiveAnnouncementTauntProbabilityLevelValue.iterator();

			int i = 0;
			while (i < randomInt)
			{
				chosenPositiveAnnouncementTaunt = (String) tauntIterator.next();

				i += (int)(Integer) levelValueIterator.next();
			}

			numberOfAttempt += 1;
		}

		alreadyProvidedPositiveAnnouncementTaunt.add(chosenPositiveAnnouncementTaunt);

		return chosenPositiveAnnouncementTaunt;
	}

	private	String provideNegativeAnnouncementTaunt()
	{
		String chosenNegativeAnnouncementTaunt = "";

		int numberOfAttempt = 1;

		while ((alreadyProvidedNegativeAnnouncementTaunt.contains(chosenNegativeAnnouncementTaunt))
		       ||
		       (chosenNegativeAnnouncementTaunt.equals("")))
		{
			if (numberOfAttempt > 100)
			{
				break;
			}

			int randomInt = randomizer.nextInt(negativeAnnouncementTauntProbabilityLevelSum);

			Iterator tauntIterator = possibleNegativeAnnouncementTaunt.iterator();
			Iterator levelValueIterator = negativeAnnouncementTauntProbabilityLevelValue.iterator();

			int i = 0;
			while (i < randomInt)
			{
				chosenNegativeAnnouncementTaunt = (String) tauntIterator.next();

				i += (int)(Integer) levelValueIterator.next();
			}

			numberOfAttempt += 1;
		}

		alreadyProvidedNegativeAnnouncementTaunt.add(chosenNegativeAnnouncementTaunt);

		return chosenNegativeAnnouncementTaunt;
	}

	private void addName(String playerNameToAdd, int playerNameProbabilityLevel)
	{
		possiblePlayerName.add(playerNameToAdd);
		playerNameProbabilityLevelValue.add(playerNameProbabilityLevel);

		playerNameProbabilityLevelSum += playerNameProbabilityLevel;
	}

	private void addNegativeCritiqueTaunt(String negativeCritiqueTauntToAdd, int negativeCritiqueTauntProbabilityLevel)
	{
		possibleNegativeCritiqueTaunt.add(negativeCritiqueTauntToAdd);
		negativeCritiqueTauntProbabilityLevelValue.add(negativeCritiqueTauntProbabilityLevel);

		negativeCritiqueTauntProbabilityLevelSum += negativeCritiqueTauntProbabilityLevel;
	}

	private void addPositiveCritiqueTaunt(String positiveCritiqueTauntToAdd, int positiveCritiqueTauntProbabilityLevel)
	{
		possiblePositiveCritiqueTaunt.add(positiveCritiqueTauntToAdd);
		positiveCritiqueTauntProbabilityLevelValue.add(positiveCritiqueTauntProbabilityLevel);

		positiveCritiqueTauntProbabilityLevelSum += positiveCritiqueTauntProbabilityLevel;
	}

	private void addPositiveAnnouncementTaunt(String positiveAnnouncementTauntToAdd, int positiveAnnouncementTauntProbabilityLevel)
	{
		possiblePositiveAnnouncementTaunt.add(positiveAnnouncementTauntToAdd);
		positiveAnnouncementTauntProbabilityLevelValue.add(positiveAnnouncementTauntProbabilityLevel);

		positiveAnnouncementTauntProbabilityLevelSum += positiveAnnouncementTauntProbabilityLevel;
	}

	private void addNegativeAnnouncementTaunt(String negativeAnnouncementTauntToAdd, int negativeAnnouncementTauntProbabilityLevel)
	{
		possibleNegativeAnnouncementTaunt.add(negativeAnnouncementTauntToAdd);
		negativeAnnouncementTauntProbabilityLevelValue.add(negativeAnnouncementTauntProbabilityLevel);

		negativeAnnouncementTauntProbabilityLevelSum += negativeAnnouncementTauntProbabilityLevel;
	}
}
