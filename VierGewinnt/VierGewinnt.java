import java.util.*;

/* A primitive brute-force-engine for Connect Four by Daniel Missal */

public class VierGewinnt
{
	public static void main(String args[])
	{
		VierGewinnt Game = new VierGewinnt();
	}

	Scanner MyScanner = new Scanner(System.in);
	String[] NameSpieler = {"", "", ""};		// Erstes Element = Dummy
	int[] TypSpieler = {0, 1, 1};			// Erstes Element = Dummy, 1 = Mensch, 0 = Computer
	String[] FarbeSpieler = {" ", "X", "O"};
	int[][] Feld;					// Feld[Spalte][Zeile], spaetere Belegungen: 0 = Leeres Feld, 1 = Spieler1, 2 = Spieler2
	int Halbzugnummer = 1;
	int Spielstatus = 0;
	int Spielfeldbreite = 7;
	int Spielfeldhoehe = 6;
	int Siegkettenlaenge = 4;
	int SpalteZug;

/* Fun */	private Humanizer humanizer = new Humanizer();

	public VierGewinnt()
	{
		Initialisierung();
		Eingaben();
		while (true)
		{
			Spielstatusanzeige();
			Spielfeldanzeige();
			if (Spielstatus > 0) Spielende();

			Zugaufforderung();

			Zugauswertung();
			Halbzugnummer += 1;
		}
	}

	void Initialisierung()
	{
		Feld = new int[Spielfeldbreite][Spielfeldhoehe];

		for (int i = 0; i < Spielfeldbreite; i++)
		for (int j = 0; j < Spielfeldhoehe; j++)
		{
			Feld[i][j] = 0;
		}
	}

	void Eingaben()
	{
		System.out.printf("\nPerfect Scalable Connect Four\n");
		System.out.printf("=============================\n\n");

		System.out.printf("Bitte geben Sie den Namen fuer (den anziehenden) Spieler 1 ein (Sofortiges Return --> Computer spielt).\n\n");

		NameSpieler[1] = MyScanner.nextLine();

		System.out.printf("\nBitte geben Sie den Namen fuer (den nachziehenden) Spieler 2 ein (Sofortiges Return --> Computer spielt).\n\n");

		NameSpieler[2] = MyScanner.nextLine();

		if (NameSpieler[1].equals(""))
		{
			TypSpieler[1] = 0;
			NameSpieler[1] = "Computer";

/* Fun */		NameSpieler[1] = humanizer.providePlayerName();
		}
		if (NameSpieler[2].equals(""))
		{
			TypSpieler[2] = 0;
			NameSpieler[2] = "Computer";

/* Fun */		NameSpieler[2] = humanizer.providePlayerName();
		}
		if (NameSpieler[1].equals(NameSpieler[2]))
		{
			NameSpieler[1] += "1";
			NameSpieler[2] += "2";
		}

		System.out.printf("\nBitte geben Sie die Anzahl der Spielfeldzeilen ein (Original: 6, Empfehlung: 4).\n\n");

		Spielfeldhoehe = MyScanner.nextInt();

		System.out.printf("\nBitte geben Sie die Anzahl der Spielfeldspalten ein (Original: 7, Empfehlung: 4).\n\n");

		Spielfeldbreite = MyScanner.nextInt();

		System.out.printf("\nBitte geben Sie die Siegkettenlaenge ein (Original: 4, Empfehlung: 4).\n\n");

		Siegkettenlaenge = MyScanner.nextInt();
	}

	void Spielstatusanzeige()
	{
		switch(Spielstatus)
		{
			case 0:
				System.out.printf("\nSpieler am Zug: %s     Halbzugnummer: %d\n\n",
				                  NameSpieler[SpielerAmZug(Halbzugnummer)] , Halbzugnummer);
				break;
			case 1:
				System.out.printf("\nSpieler am Zug: --     Benoetigte Halbzuege: %d\n\n", Halbzugnummer - 1);
				break;
			case 2:
				System.out.printf("\nSpieler am Zug: --     Benoetigte Halbzuege: %d\n\n", Halbzugnummer - 1);
				break;
			case 3:
				System.out.printf("\nSpieler am Zug: --     Benoetigte Halbzuege: %d\n\n", Halbzugnummer - 1);
		}
	}

	void Spielfeldanzeige()
	{
		System.out.printf("\n ");								// Spaltennummern
		for (int i = 1; i <= Spielfeldbreite; i++)
		{
			System.out.printf("  %2d",i);
		}
		System.out.printf("\n");

		for (int j = Spielfeldhoehe-1; j >= 0; j--)						// Spielfeld
		{
			System.out.printf("\n  |");							// Reihe
			for (int i = 0; i < Spielfeldbreite; i++)
			{
				System.out.printf(" %s |",FarbeSpieler[Feld[i][j]]);
			}
			System.out.printf("\n  -");
			for (int i = 0; i < Spielfeldbreite; i++)
			{
				System.out.printf("----");						// Trennreihe
			}
		}
		System.out.printf("\n");
	}

	void Spielende()
	{
		switch(Spielstatus)
		{
			case 1:
				System.out.printf("\nSpielergebnis: %s 1 : 0 %s - %s gewinnt.\n\n",NameSpieler[1],NameSpieler[2],NameSpieler[1]);
				break;
			case 2:
				System.out.printf("\nSpielergebnis: %s 0 : 1 %s - %s gewinnt.\n\n",NameSpieler[1],NameSpieler[2],NameSpieler[2]);
				break;
			case 3:
				System.out.printf("\nSpielergebnis: %s 0,5 : 0,5 %s - Remis.\n\n",NameSpieler[1],NameSpieler[2]);
		}

		System.exit(0);
	}

	void Zugaufforderung()
	{
		if(TypSpieler[SpielerAmZug(Halbzugnummer)] == 1)
		{
			do
			{
				System.out.printf("\n\n%s, geben Sie bitte die Nummer der Spalte ein, in die Sie setzen wollen.\n\n",
						NameSpieler[SpielerAmZug(Halbzugnummer)]);
				SpalteZug = MyScanner.nextInt() - 1;
			} while ((SpalteZug < 0) || (SpalteZug >= Spielfeldbreite) || (!ZugGueltig(SpalteZug,Feld)));
		}
		else
		{
 			Computerzug();
		}
	}

	void Zugauswertung()
	{
		int TempSAZ = SpielerAmZug(Halbzugnummer);

		Feld[SpalteZug][Stapelhoehe(SpalteZug,Feld)] = TempSAZ;

		if (Sieg(SpalteZug,Stapelhoehe(SpalteZug,Feld) - 1,Feld))							// Siegbedingungen
		{
			Spielstatus = TempSAZ;
		}

		if ((Spielstatus == 0) && (Halbzugnummer == Spielfeldbreite * Spielfeldhoehe))					// Remisbedingungen
		{
			Spielstatus = 3;
		}
	}

	int SpielerAmZug(int Halbzugnummer)
	{
		return (2 - Halbzugnummer % 2);
	}

	boolean ZugGueltig(int SpalteZug, int[][] Feld)
	{
		if (Feld[SpalteZug][Spielfeldhoehe-1] == 0) return true;
		else return false;
	}

	int Stapelhoehe(int Spalte, int[][] Feld)
	{
		int Rueckgabewert = Spielfeldhoehe;

		for (int i = 0; i < Spielfeldhoehe; i++)
		{
			if (Feld[Spalte][i] == 0)
			{
				Rueckgabewert = i;
				break;
			}
		}

		return Rueckgabewert;
	}

	boolean Sieg(int Spalte, int Zeile, int[][] Feld)
	{
		int PlatzLinks	= Spalte;
		int PlatzRechts	= Spielfeldbreite - Spalte - 1;
		int PlatzOben	= Spielfeldhoehe  - Zeile  - 1;
		int PlatzUnten	= Zeile;

		if (PlatzLinks  > (Siegkettenlaenge - 1))	PlatzLinks  = Siegkettenlaenge - 1;
		if (PlatzRechts > (Siegkettenlaenge - 1))	PlatzRechts = Siegkettenlaenge - 1;
		if (PlatzOben   > (Siegkettenlaenge - 1))	PlatzOben   = Siegkettenlaenge - 1;
		if (PlatzUnten  > (Siegkettenlaenge - 1))	PlatzUnten  = Siegkettenlaenge - 1;

		int PlatzObenLinks	= PlatzLinks;
		int PlatzUntenLinks	= PlatzLinks;
		int PlatzObenRechts	= PlatzRechts;
		int PlatzUntenRechts	= PlatzRechts;

		if (PlatzOben	< PlatzObenLinks) 	PlatzObenLinks		= PlatzOben;
		if (PlatzUnten	< PlatzUntenLinks) 	PlatzUntenLinks 	= PlatzUnten;
		if (PlatzOben	< PlatzObenRechts)	PlatzObenRechts		= PlatzOben;
		if (PlatzUnten	< PlatzUntenRechts)	PlatzUntenRechts	= PlatzUnten;

		int Zaehler = 0;
		int ZaehlfarbeAlt = -1;
		int ZaehlfarbeNeu;

		for (int i = Spalte - PlatzLinks; i <= Spalte + PlatzRechts; i++)	// 4 bzw. Siegkettenlaenge in einer Zeile?
		{
			ZaehlfarbeNeu = Feld[i][Zeile];
			if (ZaehlfarbeNeu == ZaehlfarbeAlt)
			{
				Zaehler += 1;
				if (Zaehler == Siegkettenlaenge) return true;
			}
			else
			{
				ZaehlfarbeAlt = ZaehlfarbeNeu;
				Zaehler = 1;
			}
		}

		ZaehlfarbeAlt = -1;							// 4 in einer Spalte?
		for (int j = Zeile + PlatzOben; j >= Zeile - PlatzUnten; j--)
		{
			ZaehlfarbeNeu = Feld[Spalte][j];
			if (ZaehlfarbeNeu == ZaehlfarbeAlt)
			{
				Zaehler += 1;
				if (Zaehler == Siegkettenlaenge) return true;
			}
			else
			{
				ZaehlfarbeAlt = ZaehlfarbeNeu;
				Zaehler = 1;
			}
		}

		ZaehlfarbeAlt = -1;							// 4 in einer Diagonale von oben links nach unten rechts?
		for (int i = Spalte - PlatzObenLinks, j = Zeile + PlatzObenLinks; (i <= Spalte + PlatzUntenRechts); i++, j--)
		{
			ZaehlfarbeNeu = Feld[i][j];
			if (ZaehlfarbeNeu == ZaehlfarbeAlt)
			{
				Zaehler += 1;
				if (Zaehler == Siegkettenlaenge) return true;
			}
			else
			{
				ZaehlfarbeAlt = ZaehlfarbeNeu;
				Zaehler = 1;
			}
		}

		ZaehlfarbeAlt = -1;							// 4 in einer Diagonale von unten links nach oben rechts?
		for (int i = Spalte - PlatzUntenLinks, j = Zeile - PlatzUntenLinks; (i <= Spalte + PlatzObenRechts); i++, j++)
		{
			ZaehlfarbeNeu = Feld[i][j];
			if (ZaehlfarbeNeu == ZaehlfarbeAlt)
			{
				Zaehler += 1;
				if (Zaehler == Siegkettenlaenge) return true;
			}
			else
			{
				ZaehlfarbeAlt = ZaehlfarbeNeu;
				Zaehler = 1;
			}
		}

		return false;
	}


	boolean FeldExistiert(int Spalte, int Zeile)
	{
		if ((Spalte >= 0) && (Spalte < Spielfeldbreite) && (Zeile >= 0) && (Zeile < Spielfeldhoehe))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void Computerzug()
	{
		System.out.printf("\nDer Computer berechnet einen Zug.\n");

		int[][] bf_Feld = new int[Spielfeldbreite][Spielfeldhoehe];		// brute force Feld
		for (int i = 0; i < Spielfeldbreite; i++) 
		for (int j = 0; j < Spielfeldhoehe; j++)
		{
			bf_Feld[i][j] = Feld[i][j];					// Aus dem Originalarray kopieren
		}

		int[] bf_Stapelhoehe = new int[Spielfeldbreite];			// brute force Stapelhoehe
		for (int i = 0; i < Spielfeldbreite; i++)
		{
			bf_Stapelhoehe[i] = Stapelhoehe(i,bf_Feld);			// Stapelhoehen initialisieren
		}

		int bf_HN = Halbzugnummer;						// brute force Halbzugnummer

		int[] bf_NTZS	= new int[Spielfeldbreite * Spielfeldhoehe + 2];	// brute force Naechster tieferer Zug Spalte
		int[] bf_ETB	= new int[Spielfeldbreite * Spielfeldhoehe + 2];	// brute force Extreme tiefere Bewertung
		int[] bf_AZS	= new int[Spielfeldbreite * Spielfeldhoehe + 2];	// brute force Aktueller Zug Spalte
		int[] bf_AZZ	= new int[Spielfeldbreite * Spielfeldhoehe + 2];	// brute force Aktueller Zug Zeile
		for (int i = 0; i < Spielfeldbreite * Spielfeldhoehe + 2; i++)
		{
			bf_NTZS[i]	= -1;						// Halbzugeigenschaften initialisieren
			bf_ETB[i]	= -2;
			bf_AZS[i]	= -1;
			bf_AZZ[i]	= -1;
		}

		int BZB = -2;								// brute force Bester Zug Bewertung
		int BZS = -2;								// brute force Bester Zug Spalte

/* Dev */	int plyCounter = 0;

		while (true)	// Variantenbaumbetrachtung
		{
			if (bf_NTZS[bf_HN] < Spielfeldbreite)
			{		// Falls manche von dieser Stellung ausgehenden Varianten noch nicht geprueft worden sind
				if ((bf_ETB[bf_HN] != -2) && (bf_Stapelhoehe[bf_NTZS[bf_HN]] < Spielfeldhoehe))
				{				// Eventuell ETZ vom hoeheren Zug aktualisieren
					if (bf_HN > Halbzugnummer)	// Falls nicht an der Wurzel des Variantenbaumes
					{
						if ((bf_HN % 2) == (Halbzugnummer % 2) && (bf_ETB[bf_HN] == 1))
						{				// Falls Siegvariante fuer den Zug suchenden Spieler entdeckt
							bf_HN -= 1;					// Letzten Zug zurueck setzen
							bf_Feld[bf_AZS[bf_HN]][bf_AZZ[bf_HN]] = 0;
							bf_Stapelhoehe[bf_AZS[bf_HN]] -= 1;

							if (bf_ETB[bf_HN] == -2)			// Falls hoeherer Zug noch nicht weiter
							{						// getestet
								bf_ETB[bf_HN] = 1;				// Hoeheren Zug entsprechend
							}							// abwerten
						}

						if ((bf_HN % 2) != (Halbzugnummer % 2) && (bf_ETB[bf_HN] == -1))
						{				// Falls Siegvariante fuer den nicht Zug suchenden Spieler entdeckt
							bf_HN -= 1;					// Letzten Zug zurueck setzen
							bf_Feld[bf_AZS[bf_HN]][bf_AZZ[bf_HN]] = 0;
							bf_Stapelhoehe[bf_AZS[bf_HN]] -= 1;

							if (bf_ETB[bf_HN] == -2)			// Falls hoeherer Zug noch nicht weiter
							{						// getestet
								bf_ETB[bf_HN] = -1;				// Hoeheren Zug entsprechend
							}							// abwerten

							if (bf_HN == Halbzugnummer) continue;		// Falls der schlechte Zug von der Wurzel
						}							// ausgeht, in der Tiefe verweilen
					}
					else				// Falls an der Wurzel des Variantenbaumes
					{
						if (bf_ETB[bf_HN] > BZB)	// Falls neue beste Variante entdeckt
						{
							BZB = bf_ETB[bf_HN];		// Neue beste Bewertung festhalten

							BZS = bf_NTZS[bf_HN];		// Diesen Zug als den Siegzug bestimmen

							if (BZB == 1)			// Falls sogar Siegvariante
							{
								break;				// Variantenbaumbetrachtung verlassen
							}
						}
					}
				}

				bf_NTZS[bf_HN] += 1;			// Naechste Zugmoeglichkeit bereitstellen

				if (bf_NTZS[bf_HN] == Spielfeldbreite) continue;
									// Falls dieses Feld gar nicht existiert, neuen Schleifendurchlauf beginnen

				if (bf_Stapelhoehe[bf_NTZS[bf_HN]] == Spielfeldhoehe)
				{					// Falls diese Zugmoeglichkeit ungueltig ist (z.B. auch bei bf_HN = 10)
					continue;				// Schleifendurchlauf verlassen
				}

				bf_AZS[bf_HN] = bf_NTZS[bf_HN];					// Zug merken
				bf_AZZ[bf_HN] = bf_Stapelhoehe[bf_NTZS[bf_HN]];

				bf_Feld[bf_AZS[bf_HN]][bf_AZZ[bf_HN]] = SpielerAmZug(bf_HN);	// Zug ausfuehren
				bf_Stapelhoehe[bf_NTZS[bf_HN]] += 1;

				bf_HN += 1;

/* Dev */			plyCounter += 1;

				bf_NTZS[bf_HN] = -1;						// Neue Stellung initialisieren
				bf_ETB[bf_HN] = -2;

				if (Sieg(bf_AZS[bf_HN - 1], bf_AZZ[bf_HN - 1], bf_Feld))	// Falls ein Spieler durch diesen Zug siegt
				{
					if(((bf_HN - 1) % 2) == (Halbzugnummer % 2))
					{					// Falls Halbzuege in dieser Tiefe vom Zug suchenden Spieler sind
						bf_HN -= 1;					// Letzten Zug zurueck setzen
						bf_Feld[bf_AZS[bf_HN]][bf_AZZ[bf_HN]] = 0;
						bf_Stapelhoehe[bf_AZS[bf_HN]] -= 1;

						bf_ETB[bf_HN] = 1;				// Stellung so gut wie moeglich bewerten
					}
					else					// Falls Halbzuege in dieser Tiefe nicht vom Zug suchenden Spieler sind
					{
						bf_HN -= 1;					// Letzten Zug zurueck setzen
						bf_Feld[bf_AZS[bf_HN]][bf_AZZ[bf_HN]] = 0;
						bf_Stapelhoehe[bf_AZS[bf_HN]] -= 1;

						bf_ETB[bf_HN] = -1;				// Stellung so schlecht wie moeglich bewerten
					}
				}

				if (bf_HN == Spielfeldbreite * Spielfeldhoehe + 1)	// Falls diese Variante Remis ergibt
				{
					bf_HN -= 1;					// Letzten Zug zurueck setzen
					bf_Feld[bf_AZS[bf_HN]][bf_AZZ[bf_HN]] = 0;
					bf_Stapelhoehe[bf_AZS[bf_HN]] -= 1;

					if ((((bf_HN % 2) == (Halbzugnummer % 2)) && (bf_ETB[bf_HN] < 0))	// Falls diese Variante die bisher
					    ||									// beste entdeckte Variante darstellt
					    (((bf_HN % 2) != (Halbzugnummer % 2)) && (bf_ETB[bf_HN] > 0))	//
					    ||									//
					    (bf_ETB[bf_HN] == -2))						//
					{
						bf_ETB[bf_HN] = 0;							// Bewertung neutralisieren
					}
				}
			}
			else				// Falls alle von hier ausgehenden Varianten geprueft worden sind
			{
				if((bf_HN % 2) == (Halbzugnummer % 2))	// Falls Halbzuege in dieser Tiefe vom Zug suchenden Spieler sind
				{
					if (bf_HN != Halbzugnummer)		// Falls noch nicht schlussendlich an der Wurzel
					{					// des Variantenbaumes angekommen
						bf_HN -= 1;								// Letzten Zug zurueck setzen
						bf_Feld[bf_AZS[bf_HN]][bf_AZZ[bf_HN]] = 0;
						bf_Stapelhoehe[bf_AZS[bf_HN]] -= 1;

						if ((bf_ETB[bf_HN + 1] < bf_ETB[bf_HN]) || (bf_ETB[bf_HN] == -2))	// Falls Variante extremer
						{
							bf_ETB[bf_HN] = bf_ETB[bf_HN + 1];					// Variante aufwerten
						}
					}
					else break;				// Wenn alle Varianten geprueft worden sind, Baumbetrachtung verlassen
				}
				else					// Falls Halbzuege in dieser Tiefe nicht vom Zug suchenden Spieler sind
				{
					bf_HN -= 1;									// Letzten Zug zurueck setzen
					bf_Feld[bf_AZS[bf_HN]][bf_AZZ[bf_HN]] = 0;
					bf_Stapelhoehe[bf_AZS[bf_HN]] -= 1;

					if (bf_ETB[bf_HN + 1] > bf_ETB[bf_HN])						// Falls Variante extremer
					{
						bf_ETB[bf_HN] = bf_ETB[bf_HN + 1];						// Variante aufwerten
					}	
				}
			}
		}

		SpalteZug = BZS;	// Den vom Computer am besten bewerteten Zug uebernehmen

/* Dev */	System.out.printf("\nAusgefuehrte Test-Halbzuege (Plies) : %d.\n", plyCounter);

/* Dev */	System.out.printf("\nBewertung der Stellung aus Sicht des Computers am Zuge : %d.\n", BZB);

/* Fun */	if (Halbzugnummer != 1)
/* Fun */	{
/* Fun */		System.out.printf("\n%s : \"%s\"\n", NameSpieler[SpielerAmZug(Halbzugnummer)], humanizer.provideCritiqueTaunt((float) BZB));
/* Fun */	}

		System.out.printf("\nDer Computer entscheidet sich fuer die Spalte %d.\n", SpalteZug + 1);

/* Fun */	System.out.printf("\n%s : \"%s\"\n", NameSpieler[SpielerAmZug(Halbzugnummer)], humanizer.provideAnnouncementTaunt((float) BZB));
	}
}
