Brute Force Analysis
====================

In this project (Year: 2010), I investigated the usefulness of plain brute force in popular games like "Tic Tac Toe", "Connect Four" ("Vier gewinnt") and "Chess" ("Schach") to get a feeling for the computation speed of normal computers and to see how (un)suitable brute force methods really are.  
A depth-restricted material-comparison heuristics is used in "Schach-Version-A/B/C/D" to force more sensible moves.

<img src="Diagramm_TicTacToe.png" height="100">
<img src="Diagramm_VierGewinnt.png" height="100">
<img src="Diagramm_Schach.png" height="100" width="120">
<img src="TicTacToe.png" height="100">
<img src="Schach.png" height="100">

Project Goals
-------------

- Create simple command-line interfaces for the games "Tic Tac Toe", "Connect Four" and "Chess" to play
- Make the board size (i.e., width and height) customizable for the game "Connect Four"
- For each game, create maximum-depth multilevel MinMax-algorithms that compute the perfect move given any game position
- For the game "Chess", create sensible improved versions (For details, see results section below):
    - Depth-restricted material-comparison heuristics ("Schach-Version-A")
    - Include certain aggressive moves beyond the depth-restriction ("Schach-Version-B")
    - Introduce a depth restriction for above-mentioned inclusion of aggressive moves ("Schach-Version-C")
    - Include King-chasing moves beyond the depth-restriction ("Schach-Version-D")



Results
-------

To test the algorithms, I used a machine with an 
Intel Core 2 Duo T5750 CPU @ 2.00GHz. This should be an average commercially available notebook cpu in summer 2008.

Generally, the algorithms in this project start computing the complete tree of variants in each given position anew, which means that previous results are not used in the succeeding computations.
On the other hand, when a perfect move of a subtree has been found, the rest of the subtree is omitted and the next subtree is examined. This saves a lot of time.  
The algorithm rates each position as follows:
- +1, if perfect playing will result in a win
- 0,  if perfect playing will result in a draw
- -1, if perfect playing will result in a loss

These evaluations are also printed in the game interface.  
The above-mentioned omittance of subtrees implies that computer players might choose a move that does not prevent an unavoidable loss as long as possible or does not finish the game in a minimum number of moves, if a win is safe.

### Tic Tac Toe
To compute the complete tree of variations in Tic Tac Toe, this machine does not need any noticeable time.
For the first move, 94,977 [plies](https://en.wikipedia.org/wiki/Ply_(game_theory)) are computed. The result is that the second player can always hold a draw, even if the first player only plays perfect moves.

### Connect Four
Computing the perfect first move in the original game with 6 lines and 7 columns clearly exceeded the 15 minutes mark. It is not easy to estimate, what astronomic order of magnitude of time is needed here. One problem is that we do not know of any patterns in the variation trees of chess-like games that we can exploit to extrapolate their size.  
An interesting variant of the original game contains 4 lines and 4 columns. Here, the test machine needs about 16s to find the perfect first ply and 4s for the second. This is not unexpected, since the remaining tree of variations is a subtree of the previous one. Furthermore, at 4 columns, there are 4 possible moves, which yields a good estimation for the second computation time being 1/4 of the previous. To find the perfect first move, 50,624,600 plies are computed. This variant of Connect Four also ends in a draw, if both players play perfectly.

### Chess
Since the tree of variants in Chess is essentially bigger than in Connect Four, instead of aiming for a complete computation of the perfect move, I implemented a heuristics which measures the material of both players to evaluate the moves and introduced a customizable maximum depth. 
This means that the moves of the single variants of the tree are only executed until this maximum depth is reached and then the remaining material is measured (checkmate and draw is also detected).  
This way, a multilevel MinMax-algorithm is executed. It is included in "**Schach-Version-A**".
Here, it is still possible to make the program look for a real perfect move by setting the customizable maximum depth to an impossibly high value (e.g. 1,000,000). Setting it to 4, the test machine need about 7s to compute the first move.  
One disadvantage of this version is that when the maximum depth is reached in a variation, it is not checked, what can happen directly afterwards. For example, it might be possible that a valuable piece ends up on a square, where it can be captured by a pawn. In cases like this, the final measurement of material happens too early. "**Schach-Version-B**" prevents this problem by allowing to check all deeper branches of the tree which exclusively consist of aggressive moves, i.e., capturing, giving check or promoting pawns.  
Since these exceptions exceed the maximum depth and happen quite often, this version is not satisfying. "**Schach-Version-C**" introduces a maximum depth for aggressive moves.  
One remaining weakness of versions A to C is that a king which is put in check is frequently considered as safe as long as there is a way to prevent the mate directly in the next move.
Here, it is often possible to chase the king into the final mate, which is not computed. "**Schach-Version-D**" solves a lot of these problems by allowing to check all deeper branches (up to the aggressive limit) where the chasing player gives further checks after obviation.

### Conclusion
As expected, plain brute-force methods are only sensible for small problems like Tic Tac Toe.
Restricting brute-force methods in depths as tried in Schach versions A to D is an involved undertaking and probably never worth the effort and results compared to rational approaches.
Developing these methods gave me a good understanding of the problems that chess computer pioneers had to face in the early times though and I learned a lot about encoding mechanisms as well as programming in general.



How to Play a Game
------------------

To play a game, open a terminal, change into the subdirectory of the game you want to play and start the .jar-file. Examples:  
`cd TicTacToe;        java -jar TicTacToe.jar`  
`cd VierGewinnt;      java -jar VierGewinnt.jar`  
`cd Schach-Version-A; java -jar Schach.jar`  
`cd Schach-Version-B; java -jar Schach.jar`  
`cd Schach-Version-C; java -jar Schach.jar`  
`cd Schach-Version-D; java -jar Schach.jar`  

### Translations

"Bitte geben Sie den Namen fuer (den anziehenden) Spieler 1 ein (Sofortiges Return --> Computer spielt)."  
==> "Please enter the name of the first player (Immediate Return --> Computer moves)."

"Bitte geben Sie den Namen fuer (den anziehenden) Spieler 1 ein (Sofortiges Return --> Computer spielt)."  
==> "Please enter the name of the second player (Immediate Return --> Computer moves)."

"Spieler am Zug"  
==> "Current player"

"Halbzugnummer"  
==> "Ply number"

"Playername, geben Sie bitte die Nummer der Zeile ein, in die Sie setzen wollen."  
==> "Playername, please enter the line-number of the field you want to mark."

"Playername, geben Sie bitte die Nummer der Spalte ein, in die Sie setzen wollen."  
==> "Playername, please enter the column-number of the field you want to mark."

"Der Computer berechnet einen Zug."  
==> "The machine is computing a move."

"Ausgefuehrte Test-Halbzuege (Plies)"  
==> "Executed test-plies"

"Bewertung der Stellung aus Sicht des Computers am Zuge"  
==> "Position evaluation from the perspective of the current computer player"

"Der Computer entscheidet sich fuer das Feld"  
==> "The computer chose the field"

"Benoetigte Halbzuege"  
==> "Played plies"

"Spielergebnis"  
==> "Game result

"Playername gewinnt"  
==> "Playername wins"

"Bitte geben Sie die Anzahl der Spielfeldzeilen ein (Original: 6, Empfehlung: 4)."  
==> "Please enter the number of lines (Original: 6, Recommendation: 4)."

"Bitte geben Sie die Anzahl der Spielfeldspalten ein (Original: 6, Empfehlung: 4)."  
==> "Please enter the number of columns (Original: 7, Recommendation: 4)."

"Bitte geben Sie die Siegkettenlaenge ein (Original: 4, Empfehlung: 4)."  
==> "Please enter the length of a chain of equal color needed to win the game (Original: 4, Recommendation: 4)."

"Playername, geben Sie bitte die Nummer der Spalte ein, in die Sie setzen wollen."  
==> "Playername, please enter the number of the slot, you want to insert into."

"Bitte geben Sie die Tiefe, mit der der Computer rechnen soll, in Halbzuegen an."  
==> "Please enter the maximum computation-depth in plies."

"Bitte geben Sie die Mindesttiefe, mit der der Computer rechnen soll, in Halbzuegen an (Empfehlung: 1 oder 2)."  
==> "Please enter the minimum computation-depth in plies (Recommendation: 1 or 2)."

"Bitte geben Sie die Hoechsttiefe, mit der der Computer rechnen soll, in Halbzuegen an (Empfehlung: 4, 6 oder 8)."  
==> "Please enter the maximum computation-depth in plies (Recommendation: 4, 6 or 8)."

"... bisher ausgefuehrte Halbzuege"  
==> "... already executed plies"

"derzeitige Tiefe"  
==> "current depth"
