import java.util.*;

/* A primitive brute-force-engine for Tic-Tac-Toe by Daniel Missal */

public class TicTacToe
{
	public static void main(String args[])
	{
		TicTacToe Game = new TicTacToe();
	}

	Scanner MyScanner = new Scanner(System.in);
	String[] NameSpieler = {"", "", ""};		// Erstes Element = Dummy
	int[] TypSpieler = {0, 1, 1};			// Erstes Element = Dummy, 1 = Mensch, 0 = Computer
	String[] FarbeSpieler = {" ", "X", "O"};
	int Feld[][] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};	// 0 = Leeres Feld, 1 = Spieler1, 2 = Spieler2
	int Halbzugnummer = 1;
	int Spielstatus = 0;
	int ZeileZug;
	int SpalteZug;

/* Fun */	private Humanizer humanizer = new Humanizer();

	public TicTacToe()
	{
		Eingaben();
		while (true)
		{
			Spielstatusanzeige();
			Spielfeldanzeige();
			if (Spielstatus > 0) Spielende();

			Zugaufforderung();

			Zugauswertung();
			Halbzugnummer += 1;
		}
	}

	void Eingaben()
	{
		System.out.printf("\nPerfect Tic Tac Toe\n");
		System.out.printf("===================\n\n");

		System.out.printf("Bitte geben Sie den Namen fuer (den anziehenden) Spieler 1 ein (Sofortiges Return --> Computer spielt).\n\n");

		NameSpieler[1] = MyScanner.nextLine();

		System.out.printf("\nBitte geben Sie den Namen fuer (den nachziehenden) Spieler 2 ein (Sofortiges Return --> Computer spielt).\n\n");

		NameSpieler[2] = MyScanner.nextLine();

		if (NameSpieler[1].equals(""))
		{
			TypSpieler[1] = 0;
			NameSpieler[1] = "Computer";

/* Fun */		NameSpieler[1] = humanizer.providePlayerName();
		}
		if (NameSpieler[2].equals(""))
		{
			TypSpieler[2] = 0;
			NameSpieler[2] = "Computer";

/* Fun */		NameSpieler[2] = humanizer.providePlayerName();
		}
		if (NameSpieler[1].equals(NameSpieler[2]))
		{
			NameSpieler[1] += "1";
			NameSpieler[2] += "2";
		}		
	}

	void Spielstatusanzeige()
	{
		switch(Spielstatus)
		{
			case 0:
				System.out.printf("\nSpieler am Zug: %s     Halbzugnummer: %d\n\n",
				                  NameSpieler[SpielerAmZug(Halbzugnummer)] , Halbzugnummer);
				break;
			case 1:
				System.out.printf("\nSpieler am Zug: --     Benoetigte Halbzuege: %d\n\n", Halbzugnummer - 1);
				break;
			case 2:
				System.out.printf("\nSpieler am Zug: --     Benoetigte Halbzuege: %d\n\n", Halbzugnummer - 1);
				break;
			case 3:
				System.out.printf("\nSpieler am Zug: --     Benoetigte Halbzuege: %d\n\n", Halbzugnummer - 1);
		}
	}

	void Spielfeldanzeige()
	{
		System.out.printf("\n    1   2   3\n");
		System.out.printf("\n  1 %s | %s | %s",FarbeSpieler[Feld[0][0]],FarbeSpieler[Feld[0][1]],FarbeSpieler[Feld[0][2]]);
		System.out.printf("\n    ---------");
		System.out.printf("\n  2 %s | %s | %s",FarbeSpieler[Feld[1][0]],FarbeSpieler[Feld[1][1]],FarbeSpieler[Feld[1][2]]);
		System.out.printf("\n    ---------");
		System.out.printf("\n  3 %s | %s | %s\n\n",FarbeSpieler[Feld[2][0]],FarbeSpieler[Feld[2][1]],FarbeSpieler[Feld[2][2]]);
	}

	void Spielende()
	{
		switch(Spielstatus)
		{
			case 1:
				System.out.printf("\nSpielergebnis: %s 1 : 0 %s - %s gewinnt.\n\n",NameSpieler[1],NameSpieler[2],NameSpieler[1]);
				break;
			case 2:
				System.out.printf("\nSpielergebnis: %s 0 : 1 %s - %s gewinnt.\n\n",NameSpieler[1],NameSpieler[2],NameSpieler[2]);
				break;
			case 3:
				System.out.printf("\nSpielergebnis: %s 0,5 : 0,5 %s - Remis.\n\n",NameSpieler[1],NameSpieler[2]);
		}

		System.exit(0);
	}

	void Zugaufforderung()
	{
		if(TypSpieler[SpielerAmZug(Halbzugnummer)] == 1)
		{
			do
			{
				System.out.printf("\n\n%s, geben Sie bitte die Nummer der Zeile ein, in die Sie setzen wollen.\n\n",
						NameSpieler[SpielerAmZug(Halbzugnummer)]);
				ZeileZug = MyScanner.nextInt() - 1;
				System.out.printf("\n\n%s, geben Sie bitte die Nummer der Spalte ein, in die Sie setzen wollen.\n\n",
						NameSpieler[SpielerAmZug(Halbzugnummer)]);
				SpalteZug = MyScanner.nextInt() - 1;
			} while ((ZeileZug < 0) || (ZeileZug > 2) || (SpalteZug < 0) || (SpalteZug > 2) || (!ZugGueltig(ZeileZug,SpalteZug,Feld)));
		}
		else
		{
 			Computerzug();
		}
	}

	void Zugauswertung()
	{
		int TempSAZ = SpielerAmZug(Halbzugnummer);

		Feld[ZeileZug][SpalteZug] = TempSAZ;

		if (((Feld[ZeileZug][0] == TempSAZ) && (Feld[ZeileZug][1] == TempSAZ) && (Feld[ZeileZug][2] == TempSAZ))
		    ||
		    ((Feld[0][SpalteZug] == TempSAZ) && (Feld[1][SpalteZug] == TempSAZ) && (Feld[2][SpalteZug] == TempSAZ))
		    ||
		    ((Feld[0][0] == TempSAZ) && (Feld[1][1] == TempSAZ) && (Feld[2][2] == TempSAZ))
		    ||
		    ((Feld[0][2] == TempSAZ) && (Feld[1][1] == TempSAZ) && (Feld[2][0] == TempSAZ))) 
		{
			Spielstatus = TempSAZ;
		}

		if ((Spielstatus == 0) && (Halbzugnummer == 9))
		{
			Spielstatus = 3;
		}
	}

	int SpielerAmZug(int Halbzugnummer)
	{
		return 2 - Halbzugnummer % 2;
	}

	boolean ZugGueltig(int ZeileZug,int SpalteZug, int[][] Feld)
	{
		if (Feld[ZeileZug][SpalteZug] == 0) return true;
		else return false;
	}

	void Computerzug()
	{
		System.out.printf("\nDer Computer berechnet einen Zug.\n");

		int[][] bf_Feld = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};		// brute force Feld
		for (int i = 0; i <= 2; i++) for (int j = 0; j <= 2; j++) bf_Feld[i][j] = Feld[i][j];	// Aus dem Originalarray kopieren

		int bf_HN = Halbzugnummer;					// brute force Halbzugnummer
		int[] bf_NTZZ = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};		// brute force Naechster tieferer Zug Zeile
		int[] bf_NTZS = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};	// brute force Naechster tieferer Zug Spalte
		int[] bf_ETB = {-2 ,-2 ,-2 ,-2 ,-2 ,-2 ,-2 ,-2 ,-2, -2, -2};	// brute force Extreme tiefere Bewertung
		int[] bf_AZZ = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};		// brute force Aktueller Zug Zeile
		int[] bf_AZS = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};		// brute force Aktueller Zug Spalte
		int BZB = -2;							// brute force Bester Zug Bewertung
		int BZZ = -2;							// brute force Bester Zug Zeile
		int BZS = -2;							// brute force Bester Zug Spalte

/* Dev */	int plyCounter = 0;

		while (true)	// Variantenbaumbetrachtung
		{
			if (bf_NTZZ[bf_HN] < 3) 	// Falls manche von dieser Stellung ausgehenden Varianten noch nicht geprueft worden sind
			{
				if ((bf_ETB[bf_HN] != -2) && (ZugGueltig(bf_NTZZ[bf_HN],bf_NTZS[bf_HN],bf_Feld)))
				{				// Eventuell ETZ vom hoeheren Zug aktualisieren
					if (bf_HN > Halbzugnummer)	// Falls nicht an der Wurzel des Variantenbaumes
					{
						if ((bf_HN % 2) == (Halbzugnummer % 2) && (bf_ETB[bf_HN] == 1))
						{				// Falls Siegvariante fuer den Zug suchenden Spieler entdeckt
							bf_HN -= 1;					// Letzten Zug zurueck setzen
							bf_Feld[bf_AZZ[bf_HN]][bf_AZS[bf_HN]] = 0;

							if (bf_ETB[bf_HN] == -2)			// Falls hoeherer Zug noch nicht weiter
							{						// getestet
								bf_ETB[bf_HN] = 1;				// Hoeheren Zug entsprechend
							}							// abwerten
						}

						if ((bf_HN % 2) != (Halbzugnummer % 2) && (bf_ETB[bf_HN] == -1))
						{				// Falls Siegvariante fuer den nicht Zug suchenden Spieler entdeckt
							bf_HN -= 1;					// Letzten Zug zurueck setzen
							bf_Feld[bf_AZZ[bf_HN]][bf_AZS[bf_HN]] = 0;

							if (bf_ETB[bf_HN] == -2)			// Falls hoeherer Zug noch nicht weiter
							{						// getestet
								bf_ETB[bf_HN] = -1;				// Hoeheren Zug entsprechend
							}							// abwerten

							if (bf_HN == Halbzugnummer) continue;		// Falls der schlechte Zug von der Wurzel
						}							// ausgeht, in der Tiefe verweilen
					}
					else				// Falls an der Wurzel des Variantenbaumes
					{
						if (bf_ETB[bf_HN] > BZB)	// Falls neue beste Variante entdeckt
						{
							BZB = bf_ETB[bf_HN];		// Neue beste Bewertung festhalten

							BZZ = bf_NTZZ[bf_HN];		// Diesen Zug als den Siegzug bestimmen
							BZS = bf_NTZS[bf_HN];

							if (BZB == 1)			// Falls sogar Siegvariante
							{
								break;				// Variantenbaumbetrachtung verlassen
							}
						}
					}
				}

				bf_NTZS[bf_HN] += 1;			// Naechste Zugmoeglichkeit bereitstellen
				if (bf_NTZS[bf_HN] > 2)
				{
					bf_NTZS[bf_HN] = 0;
					bf_NTZZ[bf_HN] += 1;
				}

				if (bf_NTZZ[bf_HN] > 2) continue;	// Falls dieses Feld gar nicht existiert, neuen Schleifendurchlauf beginnen

				if (!ZugGueltig(bf_NTZZ[bf_HN],bf_NTZS[bf_HN],bf_Feld))
				{					// Falls diese Zugmoeglichkeit ungueltig ist (z.B. auch bei bf_HN = 10)
					continue;				// Schleifendurchlauf verlassen
				}

				bf_AZZ[bf_HN] = bf_NTZZ[bf_HN];					// Zug merken
				bf_AZS[bf_HN] = bf_NTZS[bf_HN];

				bf_Feld[bf_NTZZ[bf_HN]][bf_NTZS[bf_HN]] = SpielerAmZug(bf_HN);	// Zug ausfuehren

				bf_HN += 1;

/* Dev */			plyCounter += 1;

				bf_NTZZ[bf_HN] = 0;						// Neue Stellung initialisieren
				bf_NTZS[bf_HN] = -1;
				bf_ETB[bf_HN] = -2;

				if (((bf_Feld[0][0] == bf_Feld[0][1]) && (bf_Feld[0][0] == bf_Feld[0][2]) && (bf_Feld[0][0] > 0)) // Erste Zeile
				    ||
				    ((bf_Feld[1][0] == bf_Feld[1][1]) && (bf_Feld[1][0] == bf_Feld[1][2]) && (bf_Feld[1][0] > 0)) // Zweite Zeile
				    ||
				    ((bf_Feld[2][0] == bf_Feld[2][1]) && (bf_Feld[2][0] == bf_Feld[2][2]) && (bf_Feld[2][0] > 0)) // Dritte Zeile
				    ||
				    ((bf_Feld[0][0] == bf_Feld[1][0]) && (bf_Feld[0][0] == bf_Feld[2][0]) && (bf_Feld[0][0] > 0)) // Erste Spalte
				    ||
				    ((bf_Feld[0][1] == bf_Feld[1][1]) && (bf_Feld[0][1] == bf_Feld[2][1]) && (bf_Feld[0][1] > 0)) // Zweite Spalte
				    ||
				    ((bf_Feld[0][2] == bf_Feld[1][2]) && (bf_Feld[0][2] == bf_Feld[2][2]) && (bf_Feld[0][2] > 0)) // Dritte Spalte
				    ||
				    ((bf_Feld[0][0] == bf_Feld[1][1]) && (bf_Feld[0][0] == bf_Feld[2][2]) && (bf_Feld[0][0] > 0))  // Hauptdiagonale
				    ||
				    ((bf_Feld[0][2] == bf_Feld[1][1]) && (bf_Feld[0][2] == bf_Feld[2][0]) && (bf_Feld[0][2] > 0))) // Nebendiagonale
				{			// Falls ein Spieler durch diesen Zug siegt

					if(((bf_HN - 1) % 2) == (Halbzugnummer % 2))
					{				// Falls Halbzuege in dieser Tiefe vom Zug suchenden Spieler sind
						bf_HN -= 1;					// Letzten Zug zurueck setzen
						bf_Feld[bf_AZZ[bf_HN]][bf_AZS[bf_HN]] = 0;

						bf_ETB[bf_HN] = 1;				// Stellung so gut wie moeglich bewerten
					}
					else					// Falls Halbzuege in dieser Tiefe nicht vom Zug suchenden Spieler sind
					{
						bf_HN -= 1;					// Letzten Zug zurueck setzen
						bf_Feld[bf_AZZ[bf_HN]][bf_AZS[bf_HN]] = 0;

						bf_ETB[bf_HN] = -1;				// Stellung so schlecht wie moeglich bewerten
					}
				}

				if (bf_HN == 10)	// Falls diese Variante Remis ergibt
				{
					bf_HN -= 1;					// Letzten Zug zurueck setzen
					bf_Feld[bf_AZZ[bf_HN]][bf_AZS[bf_HN]] = 0;

					if ((((bf_HN % 2) == (Halbzugnummer % 2)) && (bf_ETB[bf_HN] < 0))	// Falls diese Variante die bisher
					    ||									// beste entdeckte Variante darstellt
					    (((bf_HN % 2) != (Halbzugnummer % 2)) && (bf_ETB[bf_HN] > 0))	//
					    ||									//
					    (bf_ETB[bf_HN] == -2))						//
					{
						bf_ETB[bf_HN] = 0;							// Bewertung neutralisieren
					}
				}
			}
			else				// Falls alle von hier ausgehenden Varianten geprueft worden sind
			{
				if((bf_HN % 2) == (Halbzugnummer % 2))	// Falls Halbzuege in dieser Tiefe vom Zug suchenden Spieler sind
				{
					if (bf_HN != Halbzugnummer)		// Falls noch nicht schlussendlich an der Wurzel
					{					// des Variantenbaumes angekommen
						bf_HN -= 1;								// Letzten Zug zurueck setzen
						bf_Feld[bf_AZZ[bf_HN]][bf_AZS[bf_HN]] = 0;

						if ((bf_ETB[bf_HN + 1] < bf_ETB[bf_HN]) || (bf_ETB[bf_HN] == -2))	// Falls Variante extremer
						{
							bf_ETB[bf_HN] = bf_ETB[bf_HN + 1];					// Variante aufwerten
						}
					}
					else break;				// Wenn alle Varianten geprueft worden sind, Baumbetrachtung verlassen
				}
				else					// Falls Halbzuege in dieser Tiefe nicht vom Zug suchenden Spieler sind
				{
					bf_HN -= 1;									// Letzten Zug zurueck setzen
					bf_Feld[bf_AZZ[bf_HN]][bf_AZS[bf_HN]] = 0;

					if (bf_ETB[bf_HN + 1] > bf_ETB[bf_HN])						// Falls Variante extremer
					{
						bf_ETB[bf_HN] = bf_ETB[bf_HN + 1];						// Variante aufwerten
					}	
				}
			}
		}

		ZeileZug = BZZ;		// Den vom Computer am besten bewerteten Zug uebernehmen
		SpalteZug = BZS;

/* Dev */	System.out.printf("\nAusgefuehrte Test-Halbzuege (Plies) : %d.\n",plyCounter);

/* Dev */	System.out.printf("\nBewertung der Stellung aus Sicht des Computers am Zuge : %d.\n",BZB);

/* Fun */	if (Halbzugnummer != 1)
/* Fun */	{
/* Fun */		System.out.printf("\n%s : \"%s\"\n",NameSpieler[SpielerAmZug(Halbzugnummer)],humanizer.provideCritiqueTaunt((float) BZB));
/* Fun */	}

		System.out.printf("\nDer Computer entscheidet sich fuer das Feld (%d,%d).\n",ZeileZug + 1,SpalteZug + 1);

/* Fun */	System.out.printf("\n%s : \"%s\"\n",NameSpieler[SpielerAmZug(Halbzugnummer)],humanizer.provideAnnouncementTaunt((float) BZB));
	}
}
