import java.util.*;

/* A primitive depth-restricted brute-force-engine for chess by Daniel Missal */

public class Schach
{
	public static void main(String args[])
	{
		Schach Game = new Schach();
	}

	Scanner MyScanner = new Scanner(System.in);
	String[] NameSpieler = {"", "", ""};		// Erstes Element = Dummy
	int[] TypSpieler = {0, 1, 1};			// Erstes Element = Dummy; 1 = Mensch, 0 = Computer

	int MaximaleZugAnzahl			= 5899;		// Laut Thomas Rayner Dawson (1889 - 1951, Gross Brittanien)
	int MaximaleZugmoeglichkeitenAnzahl	= 257;		// Obere Schranke laut Dr. Klaus Wich (Uni Stuttgart)

	int Rechentiefe;

	int[][] FeldSpieler;	// FeldSpieler[Linie][Reihe]; 0 = Leeres Feld, 1 = Spieler1, 2 = Spieler2
	int[][] FeldSteinTyp;	// FeldSteinTyp[Linie][Reihe]; 1 = Bauer, 2 = Springer, 3 = Laeufer, 4 = Turm, 5 = Dame, 6 = Koenig
	int[][] FeldStein;	// FeldStein[Linie][Reihe]; SteinNummer
	int[][] SteinLinie;	// SteinLinie[Spieler][SteinNummer]
	int[][] SteinReihe;	// SteinReihe[Spieler][SteinNummer]
	int[][] SteinTyp;	// SteinTyp[Spieler][SteinNummer]; 1 = Bauer, 2 = Springer, 3 = Laeufer, 4 = Turm, 5 = Dame, 6 = Koenig
	int[] EnPassantLinie = {0};
	boolean[] KRNMFW = new boolean[MaximaleZugAnzahl + 1];	// KRMFW bedeutet "Kleine Rochade noch moeglich fuer weiss"
	boolean[] GRNMFW = new boolean[MaximaleZugAnzahl + 1];	// GRMFW bedeutet "Grosse Rochade noch moeglich fuer weiss"
	boolean[] KRNMFS = new boolean[MaximaleZugAnzahl + 1];	// KRMFS bedeutet "Kleine Rochade noch moeglich fuer schwarz"
	boolean[] GRNMFS = new boolean[MaximaleZugAnzahl + 1];	// GRMFS bedeutet "Grosse Rochade noch moeglich fuer schwarz"

	int[] MZ = new int[MaximaleZugAnzahl + 1];		// MZ    bedeutet "Materialziffer"
	int[] HOBUS = new int[MaximaleZugAnzahl + 1];		// HOBUS bedeutet "Halbzuege ohne Bauernbewegung und Schlagen"
	int[][] SIDN = new int[MaximaleZugAnzahl + 1][9];	// SIDN  bedeutet "Stellungsidentifikationsnummer"
	int[] SWA = new int[MaximaleZugAnzahl + 1];		// SWA   bedeutet "Stellungswiederholungsanzahl"

	int Halbzugnummer = 1;
	int Spielstatus = 0;

	int[][] ZugTyp			= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
	int[][] ZugStein		= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
	int[][] ZugUmwandlungssteintyp	= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
	int[][] ZugGeschlagenerSteinTyp	= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
	int[][] ZugGeschlagenerStein	= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
	int[][] ZugSteinTyp		= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
	int[][] ZugStartfeldLinie	= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
	int[][] ZugStartfeldReihe	= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
	int[][] ZugZielfeldLinie	= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
	int[][] ZugZielfeldReihe	= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];

	int Zugmoeglichkeiten;
	boolean[] SchachGebot = {false};

	int NummerZug;

/* Fun */	private Humanizer humanizer = new Humanizer();

	public Schach()
	{
		Initialisierung();
		Eingaben();
		while (true)
		{
			Spielstatusanzeige();
			Spielfeldanzeige();
			if (Spielstatus > 0) Spielende();

			Zugaufforderung();

			Zugauswertung();
			Halbzugnummer += 1;
		}
	}

	void Initialisierung()
	{
		KRNMFW[1]		= true;
		GRNMFW[1]		= true;
		KRNMFS[1]		= true;
		GRNMFS[1]		= true;

/*
//	Ein paar Stellungstests:

SteinLinie	= new int[3][17];
SteinReihe	= new int[3][17];
SteinTyp	= new int[3][17];

FeldSpieler	= new int[9][9];
FeldSteinTyp	= new int[9][9];

SteinLinie[1][1] = 5; SteinReihe[1][1] = 1; SteinTyp[1][1] = 6; FeldSpieler[5][1] = 1; FeldSteinTyp[5][1] = 6; // Weisser Koenig
SteinLinie[1][2] = 7; SteinReihe[1][2] = 5; SteinTyp[1][2] = 2; FeldSpieler[7][5] = 1; FeldSteinTyp[7][5] = 2; // Weisser Springer
//SteinLinie[1][3] = 4; SteinReihe[1][3] = 1; SteinTyp[1][3] = 3; FeldSpieler[4][1] = 1; FeldSteinTyp[4][1] = 3; // Weisser Laeufer
SteinLinie[1][4] = 5; SteinReihe[1][4] = 4; SteinTyp[1][4] = 1; FeldSpieler[5][4] = 1; FeldSteinTyp[5][4] = 1; // Weisser Bauer
//SteinLinie[1][5] = 6; SteinReihe[1][5] = 6; SteinTyp[1][5] = 4; FeldSpieler[6][6] = 1; FeldSteinTyp[6][6] = 4; // Weisser Turm
SteinLinie[1][6] = 5; SteinReihe[1][6] = 3; SteinTyp[1][6] = 5; FeldSpieler[5][3] = 1; FeldSteinTyp[5][3] = 5; // Weisse  Dame



SteinLinie[2][1] = 5; SteinReihe[2][1] = 8; SteinTyp[2][1] = 6; FeldSpieler[5][8] = 2; FeldSteinTyp[5][8] = 6; // Schwarzer Koenig
SteinLinie[2][2] = 1; SteinReihe[2][2] = 8; SteinTyp[2][2] = 4; FeldSpieler[1][8] = 2; FeldSteinTyp[1][8] = 4; // Schwarzer Turm
SteinLinie[2][4] = 4; SteinReihe[2][4] = 4; SteinTyp[2][4] = 1; FeldSpieler[4][4] = 2; FeldSteinTyp[4][4] = 1; // Schwarzer Bauer

//	Ende der Tests.
*/

		FeldSpieler	= new int[9][9];
		FeldSteinTyp	= new int[9][9];
		FeldStein	= new int[9][9];

		for (int i = 0; i < 9; i++)
		for (int j = 0; j < 9; j++)
		{
			FeldSpieler[i][j] = 0;
			FeldSteinTyp[i][j] = 0;
			FeldStein[i][j] = 0;
		}

		for (int i = 1; i < 9; i++)
		for (int j = 1; j < 3; j++)
		{
			FeldSpieler[i][j] = 1;
		}

		for (int i = 1; i < 9; i++)
		for (int j = 7; j < 9; j++)
		{
			FeldSpieler[i][j] = 2;
		}

		for (int i = 1; i < 9; i++)
		{
			FeldSteinTyp[i][2] = 1;
			FeldSteinTyp[i][7] = 1;
		}

		FeldSteinTyp[1][1] = 4;
		FeldSteinTyp[2][1] = 2;
		FeldSteinTyp[3][1] = 3;
		FeldSteinTyp[4][1] = 5;
		FeldSteinTyp[5][1] = 6;
		FeldSteinTyp[6][1] = 3;
		FeldSteinTyp[7][1] = 2;
		FeldSteinTyp[8][1] = 4;

		FeldSteinTyp[1][8] = 4;
		FeldSteinTyp[2][8] = 2;
		FeldSteinTyp[3][8] = 3;
		FeldSteinTyp[4][8] = 5;
		FeldSteinTyp[5][8] = 6;
		FeldSteinTyp[6][8] = 3;
		FeldSteinTyp[7][8] = 2;
		FeldSteinTyp[8][8] = 4;

		SteinLinie	= new int[3][17];
		SteinReihe	= new int[3][17];
		SteinTyp	= new int[3][17];

		SteinLinie[1][1]  = 5; SteinReihe[1][1]  = 1; SteinTyp[1][1]  = 6; FeldStein[5][1] = 1;		// Weisser Koenig
		SteinLinie[1][2]  = 4; SteinReihe[1][2]  = 1; SteinTyp[1][2]  = 5; FeldStein[4][1] = 2;		// Weisse  Dame
		SteinLinie[1][3]  = 1; SteinReihe[1][3]  = 1; SteinTyp[1][3]  = 4; FeldStein[1][1] = 3;		// Weisser Turm
		SteinLinie[1][4]  = 8; SteinReihe[1][4]  = 1; SteinTyp[1][4]  = 4; FeldStein[8][1] = 4;		// Weisser Turm
		SteinLinie[1][5]  = 3; SteinReihe[1][5]  = 1; SteinTyp[1][5]  = 3; FeldStein[3][1] = 5;		// Weisser Laeufer
		SteinLinie[1][6]  = 6; SteinReihe[1][6]  = 1; SteinTyp[1][6]  = 3; FeldStein[6][1] = 6;		// Weisser Laeufer
		SteinLinie[1][7]  = 2; SteinReihe[1][7]  = 1; SteinTyp[1][7]  = 2; FeldStein[2][1] = 7;		// Weisser Springer
		SteinLinie[1][8]  = 7; SteinReihe[1][8]  = 1; SteinTyp[1][8]  = 2; FeldStein[7][1] = 8;		// Weisser Springer
		SteinLinie[1][9]  = 1; SteinReihe[1][9]  = 2; SteinTyp[1][9]  = 1; FeldStein[1][2] = 9;		// Weisser Bauer
		SteinLinie[1][10] = 2; SteinReihe[1][10] = 2; SteinTyp[1][10] = 1; FeldStein[2][2] = 10;	// Weisser Bauer
		SteinLinie[1][11] = 3; SteinReihe[1][11] = 2; SteinTyp[1][11] = 1; FeldStein[3][2] = 11;	// Weisser Bauer
		SteinLinie[1][12] = 4; SteinReihe[1][12] = 2; SteinTyp[1][12] = 1; FeldStein[4][2] = 12;	// Weisser Bauer
		SteinLinie[1][13] = 5; SteinReihe[1][13] = 2; SteinTyp[1][13] = 1; FeldStein[5][2] = 13;	// Weisser Bauer
		SteinLinie[1][14] = 6; SteinReihe[1][14] = 2; SteinTyp[1][14] = 1; FeldStein[6][2] = 14;	// Weisser Bauer
		SteinLinie[1][15] = 7; SteinReihe[1][15] = 2; SteinTyp[1][15] = 1; FeldStein[7][2] = 15;	// Weisser Bauer
		SteinLinie[1][16] = 8; SteinReihe[1][16] = 2; SteinTyp[1][16] = 1; FeldStein[8][2] = 16;	// Weisser Bauer

		SteinLinie[2][1]  = 5; SteinReihe[2][1]  = 8; SteinTyp[2][1]  = 6; FeldStein[5][8] = 1;		// Schwarzer Koenig
		SteinLinie[2][2]  = 4; SteinReihe[2][2]  = 8; SteinTyp[2][2]  = 5; FeldStein[4][8] = 2;		// Schwarze  Dame
		SteinLinie[2][3]  = 1; SteinReihe[2][3]  = 8; SteinTyp[2][3]  = 4; FeldStein[1][8] = 3;		// Schwarzer Turm
		SteinLinie[2][4]  = 8; SteinReihe[2][4]  = 8; SteinTyp[2][4]  = 4; FeldStein[8][8] = 4;		// Schwarzer Turm
		SteinLinie[2][5]  = 3; SteinReihe[2][5]  = 8; SteinTyp[2][5]  = 3; FeldStein[3][8] = 5;		// Schwarzer Laeufer
		SteinLinie[2][6]  = 6; SteinReihe[2][6]  = 8; SteinTyp[2][6]  = 3; FeldStein[6][8] = 6;		// Schwarzer Laeufer
		SteinLinie[2][7]  = 2; SteinReihe[2][7]  = 8; SteinTyp[2][7]  = 2; FeldStein[2][8] = 7;		// Schwarzer Springer
		SteinLinie[2][8]  = 7; SteinReihe[2][8]  = 8; SteinTyp[2][8]  = 2; FeldStein[7][8] = 8;		// Schwarzer Springer
		SteinLinie[2][9]  = 1; SteinReihe[2][9]  = 7; SteinTyp[2][9]  = 1; FeldStein[1][7] = 9;		// Schwarzer Bauer
		SteinLinie[2][10] = 2; SteinReihe[2][10] = 7; SteinTyp[2][10] = 1; FeldStein[2][7] = 10;	// Schwarzer Bauer
		SteinLinie[2][11] = 3; SteinReihe[2][11] = 7; SteinTyp[2][11] = 1; FeldStein[3][7] = 11;	// Schwarzer Bauer
		SteinLinie[2][12] = 4; SteinReihe[2][12] = 7; SteinTyp[2][12] = 1; FeldStein[4][7] = 12;	// Schwarzer Bauer
		SteinLinie[2][13] = 5; SteinReihe[2][13] = 7; SteinTyp[2][13] = 1; FeldStein[5][7] = 13;	// Schwarzer Bauer
		SteinLinie[2][14] = 6; SteinReihe[2][14] = 7; SteinTyp[2][14] = 1; FeldStein[6][7] = 14;	// Schwarzer Bauer
		SteinLinie[2][15] = 7; SteinReihe[2][15] = 7; SteinTyp[2][15] = 1; FeldStein[7][7] = 15;	// Schwarzer Bauer
		SteinLinie[2][16] = 8; SteinReihe[2][16] = 7; SteinTyp[2][16] = 1; FeldStein[8][7] = 16;	// Schwarzer Bauer

		MZ[1] = 52;	// 52 = 2 * (11 * 2 + 4)

		SSBZ(FeldSpieler, FeldSteinTyp,
		     EnPassantLinie, KRNMFW, GRNMFW, KRNMFS, GRNMFS,
		     1, 1,
		     SIDN, SWA);
	}

	void Eingaben()
	{
		System.out.printf("\nPerfect depth-restricted Chess\n");
		System.out.printf("==============================\n\n");

		System.out.printf("\nBitte geben Sie den Namen fuer (den anziehenden) Spieler 1 ein (Sofortiges Return --> Computer spielt).\n\n");

		NameSpieler[1] = MyScanner.nextLine();

		System.out.printf("\nBitte geben Sie den Namen fuer (den nachziehenden) Spieler 2 ein (Sofortiges Return --> Computer spielt).\n\n"
				 );

		NameSpieler[2] = MyScanner.nextLine();

		if (NameSpieler[1].equals(""))
		{
			TypSpieler[1] = 0;
			NameSpieler[1] = "Computer";

/* Fun */		NameSpieler[1] = humanizer.providePlayerName();
		}
		if (NameSpieler[2].equals(""))
		{
			TypSpieler[2] = 0;
			NameSpieler[2] = "Computer";

/* Fun */		NameSpieler[2] = humanizer.providePlayerName();
		}
		if (NameSpieler[1].equals(NameSpieler[2]))
		{
			NameSpieler[1] += "1";
			NameSpieler[2] += "2";
		}

		if ((TypSpieler[1] == 0) || (TypSpieler[2] == 0))
		{
			do
			{
				System.out.printf("\nBitte geben Sie die Tiefe, mit der der Computer rechnen soll, in Halbzuegen an.\n\n");
				Rechentiefe = MyScanner.nextInt();
			} while (Rechentiefe < 1);
		}
	}

	void Spielstatusanzeige()
	{
		switch(Spielstatus)
		{
			case 0:
				System.out.printf("\n\n\nSpieler am Zug: %s     Halbzugnummer: %d\n\n",
				                  NameSpieler[SpielerAmZug(Halbzugnummer)] , Halbzugnummer);
				break;
			case 1:
				System.out.printf("\n\n\nSpieler am Zug: --     Benoetigte Halbzuege: %d\n\n", Halbzugnummer - 1);
				break;
			case 2:
				System.out.printf("\n\n\nSpieler am Zug: --     Benoetigte Halbzuege: %d\n\n", Halbzugnummer - 1);
				break;
			case 3:
				System.out.printf("\n\n\nSpieler am Zug: --     Benoetigte Halbzuege: %d\n\n", Halbzugnummer - 1);
		}
	}

	void Spielfeldanzeige()
	{
		System.out.printf("\n      A  B  C  D  E  F  G  H");
		System.out.printf("\n");
		System.out.printf("\n    ++++++++++++++++++++++++++");
		System.out.printf("\n    +   ###   ###   ###   ###+");
		System.out.printf("\n  8 + %c #%c# %c #%c# %c #%c# %c #%c#+ 8",FB(1,8),FB(2,8),FB(3,8),FB(4,8),FB(5,8),FB(6,8),FB(7,8),FB(8,8));
		System.out.printf("\n    +   ###   ###   ###   ###+");
		System.out.printf("\n    +###   ###   ###   ###   +");
		System.out.printf("\n  7 +#%c# %c #%c# %c #%c# %c #%c# %c + 7",FB(1,7),FB(2,7),FB(3,7),FB(4,7),FB(5,7),FB(6,7),FB(7,7),FB(8,7));
		System.out.printf("\n    +###   ###   ###   ###   +");
		System.out.printf("\n    +   ###   ###   ###   ###+");
		System.out.printf("\n  6 + %c #%c# %c #%c# %c #%c# %c #%c#+ 6",FB(1,6),FB(2,6),FB(3,6),FB(4,6),FB(5,6),FB(6,6),FB(7,6),FB(8,6));
		System.out.printf("\n    +   ###   ###   ###   ###+");
		System.out.printf("\n    +###   ###   ###   ###   +");
		System.out.printf("\n  5 +#%c# %c #%c# %c #%c# %c #%c# %c + 5",FB(1,5),FB(2,5),FB(3,5),FB(4,5),FB(5,5),FB(6,5),FB(7,5),FB(8,5));
		System.out.printf("\n    +###   ###   ###   ###   +");
		System.out.printf("\n    +   ###   ###   ###   ###+");
		System.out.printf("\n  4 + %c #%c# %c #%c# %c #%c# %c #%c#+ 4",FB(1,4),FB(2,4),FB(3,4),FB(4,4),FB(5,4),FB(6,4),FB(7,4),FB(8,4));
		System.out.printf("\n    +   ###   ###   ###   ###+");
		System.out.printf("\n    +###   ###   ###   ###   +");
		System.out.printf("\n  3 +#%c# %c #%c# %c #%c# %c #%c# %c + 3",FB(1,3),FB(2,3),FB(3,3),FB(4,3),FB(5,3),FB(6,3),FB(7,3),FB(8,3));
		System.out.printf("\n    +###   ###   ###   ###   +");
		System.out.printf("\n    +   ###   ###   ###   ###+");
		System.out.printf("\n  2 + %c #%c# %c #%c# %c #%c# %c #%c#+ 2",FB(1,2),FB(2,2),FB(3,2),FB(4,2),FB(5,2),FB(6,2),FB(7,2),FB(8,2));
		System.out.printf("\n    +   ###   ###   ###   ###+");
		System.out.printf("\n    +###   ###   ###   ###   +");
		System.out.printf("\n  1 +#%c# %c #%c# %c #%c# %c #%c# %c + 1",FB(1,1),FB(2,1),FB(3,1),FB(4,1),FB(5,1),FB(6,1),FB(7,1),FB(8,1));
		System.out.printf("\n    +###   ###   ###   ###   +");
		System.out.printf("\n    ++++++++++++++++++++++++++");
		System.out.printf("\n");
		System.out.printf("\n      A  B  C  D  E  F  G  H\n");
	}

	void Spielende()
	{
		switch(Spielstatus)
		{
			case 1:
				System.out.printf("\n\nSpielergebnis: %s 1 : 0 %s - %s gewinnt.\n\n\n",
						  NameSpieler[1],NameSpieler[2],NameSpieler[1]);
				break;
			case 2:
				System.out.printf("\n\nSpielergebnis: %s 0 : 1 %s - %s gewinnt.\n\n\n",
						  NameSpieler[1],NameSpieler[2],NameSpieler[2]);
				break;
			case 3:
				System.out.printf("\n\nSpielergebnis: %s 0,5 : 0,5 %s - Remis.\n\n\n",
						  NameSpieler[1],NameSpieler[2]);
		}

		System.exit(0);
	}

	void Zugaufforderung()
	{
		Zugmoeglichkeiten = Zugmoeglichkeiten(FeldSpieler, FeldSteinTyp, FeldStein, SteinLinie, SteinReihe, SteinTyp,
						      EnPassantLinie, KRNMFW, GRNMFW, KRNMFS, GRNMFS,
						      ZugTyp, ZugSteinTyp, ZugUmwandlungssteintyp,
						      ZugGeschlagenerSteinTyp, ZugGeschlagenerStein, ZugStein,
						      ZugStartfeldLinie, ZugStartfeldReihe, ZugZielfeldLinie, ZugZielfeldReihe,
						      SpielerAmZug(Halbzugnummer), Halbzugnummer);

		if(TypSpieler[SpielerAmZug(Halbzugnummer)] == 1)
		{
			Zugauflistung();
			do
			{
				System.out.printf("\n\n%s, geben Sie bitte die Nummer des Zuges ein, den Sie ausfuehren wollen.\n\n",
						NameSpieler[SpielerAmZug(Halbzugnummer)]);
				NummerZug = MyScanner.nextInt() - 1;
			} while ((NummerZug < 0) || (NummerZug >= Zugmoeglichkeiten));
		}
		else
		{
			Computerzug();
		}
	}

	void Zugauswertung()
	{
		int TempSAZ = SpielerAmZug(Halbzugnummer);	// Temp bedeutet "temporaer", SAZ bedeutet "Spieler am Zug"
		int Temp_HN_Ink = Halbzugnummer + 1;			// HN   bedeutet "Halbzugnummer", Ink bedeutet "Inkrement"

		ZugAusfuehrung(FeldSpieler, FeldSteinTyp, FeldStein, SteinLinie, SteinReihe, SteinTyp,
			       EnPassantLinie, KRNMFW, GRNMFW, KRNMFS, GRNMFS, SchachGebot, HOBUS, MZ,
			       ZugTyp, ZugSteinTyp, ZugUmwandlungssteintyp,
			       ZugGeschlagenerSteinTyp, ZugGeschlagenerStein, ZugStein,
			       ZugStartfeldLinie, ZugStartfeldReihe, ZugZielfeldLinie, ZugZielfeldReihe,
			       TempSAZ, Halbzugnummer, NummerZug);

		if ((SchachGebot[0] == true)			// Siegbedingungen
		    && (Zugmoeglichkeiten(FeldSpieler, FeldSteinTyp, FeldStein, SteinLinie, SteinReihe, SteinTyp,
					  EnPassantLinie, KRNMFW, GRNMFW, KRNMFS, GRNMFS,
					  ZugTyp, ZugSteinTyp, ZugUmwandlungssteintyp,
					  ZugGeschlagenerSteinTyp, ZugGeschlagenerStein, ZugStein,
					  ZugStartfeldLinie, ZugStartfeldReihe, ZugZielfeldLinie, ZugZielfeldReihe,
					  3 - TempSAZ, Temp_HN_Ink)
			== 0)
		   )		
		{
			Spielstatus = TempSAZ;
		}

		if (((SchachGebot[0] == false)			// Remisbedingungen
		     && (Zugmoeglichkeiten(FeldSpieler, FeldSteinTyp, FeldStein, SteinLinie, SteinReihe, SteinTyp,
					  EnPassantLinie, KRNMFW, GRNMFW, KRNMFS, GRNMFS,
					  ZugTyp, ZugSteinTyp, ZugUmwandlungssteintyp,
					  ZugGeschlagenerSteinTyp, ZugGeschlagenerStein, ZugStein,
					  ZugStartfeldLinie, ZugStartfeldReihe, ZugZielfeldLinie, ZugZielfeldReihe,
					  3 - TempSAZ, Temp_HN_Ink)
			 == 0))
		    ||
		    ((Spielstatus == 0) && (HOBUS[Temp_HN_Ink] == 100))
		    ||
		    (MZ[Temp_HN_Ink] == 1)
		    ||
		    (SSBZ(FeldSpieler, FeldSteinTyp,
			  EnPassantLinie, KRNMFW, GRNMFW, KRNMFS, GRNMFS,
			  3 - TempSAZ, Temp_HN_Ink,
			  SIDN, SWA)
		     == true)
		   )
		{
			Spielstatus = 3;
		}
	}

	int SpielerAmZug(int Halbzugnummer)
	{
		return (2 - Halbzugnummer % 2);
	}

	int FeldFarbe(int Linie, int Reihe)
	{
		if (((Linie + Reihe) % 2) == 0) return 0;
		else return 1;
	}

	char FB(int Linie, int Reihe)	// FB bedeutet "Figurenbild"
	{
		if (FeldSpieler[Linie][Reihe] == 0)
		{
			if (FeldFarbe(Linie,Reihe) == 0) return '#';
			else return ' ';
		}
		if (FeldSpieler[Linie][Reihe] == 1)
		{
			if (FeldSteinTyp[Linie][Reihe] == 1) return 'B';
			if (FeldSteinTyp[Linie][Reihe] == 2) return 'S';
			if (FeldSteinTyp[Linie][Reihe] == 3) return 'L';
			if (FeldSteinTyp[Linie][Reihe] == 4) return 'T';
			if (FeldSteinTyp[Linie][Reihe] == 5) return 'D';
			if (FeldSteinTyp[Linie][Reihe] == 6) return 'K';
		}
		if (FeldSpieler[Linie][Reihe] == 2)
		{
			if (FeldSteinTyp[Linie][Reihe] == 1) return 'b';
			if (FeldSteinTyp[Linie][Reihe] == 2) return 's';
			if (FeldSteinTyp[Linie][Reihe] == 3) return 'l';
			if (FeldSteinTyp[Linie][Reihe] == 4) return 't';
			if (FeldSteinTyp[Linie][Reihe] == 5) return 'd';
			if (FeldSteinTyp[Linie][Reihe] == 6) return 'k';
		}

		return '?';
	}

	boolean FeldExistiert(int Linie, int Reihe)
	{
		if ((Linie >= 1) && (Linie <= 8) && (Reihe >= 1) && (Reihe <= 8)) return true;
		else return false;
	}

	boolean KoenigAngegriffen(int[][] FeldSpieler, int[][] FeldSteinTyp, int Koenigslinie, int Koenigsreihe, int Spieler)
	{
		// Koenigsabstaende pruefen

		if ((FeldExistiert(Koenigslinie + 1, Koenigsreihe))				// rechts vom Koenig?
		    && (FeldSpieler[Koenigslinie + 1][Koenigsreihe] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie + 1][Koenigsreihe] == 6))
		return true;
		if ((FeldExistiert(Koenigslinie + 1, Koenigsreihe - 1))				// unten rechts vom Koenig?
		    && (FeldSpieler[Koenigslinie + 1][Koenigsreihe - 1] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie + 1][Koenigsreihe - 1] == 6))
		return true;
		if ((FeldExistiert(Koenigslinie, Koenigsreihe - 1))				// unter dem Koenig?
		    && (FeldSpieler[Koenigslinie][Koenigsreihe - 1] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie][Koenigsreihe - 1] == 6))
		return true;
		if ((FeldExistiert(Koenigslinie - 1, Koenigsreihe - 1))				// unten links vom Koenig?
		    && (FeldSpieler[Koenigslinie - 1][Koenigsreihe - 1] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie - 1][Koenigsreihe - 1] == 6))
		return true;
		if ((FeldExistiert(Koenigslinie - 1, Koenigsreihe))				// links vom Koenig?
		    && (FeldSpieler[Koenigslinie - 1][Koenigsreihe] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie - 1][Koenigsreihe] == 6))
		return true;
		if ((FeldExistiert(Koenigslinie - 1, Koenigsreihe + 1))				// oben links vom Koenig?
		    && (FeldSpieler[Koenigslinie - 1][Koenigsreihe + 1] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie - 1][Koenigsreihe + 1] == 6))
		return true;
		if ((FeldExistiert(Koenigslinie, Koenigsreihe + 1))				// ueber dem Koenig?
		    && (FeldSpieler[Koenigslinie][Koenigsreihe + 1] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie][Koenigsreihe + 1] == 6))
		return true;
		if ((FeldExistiert(Koenigslinie + 1, Koenigsreihe + 1))				// oben rechts vom Koenig?
		    && (FeldSpieler[Koenigslinie + 1][Koenigsreihe + 1] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie + 1][Koenigsreihe + 1] == 6))
		return true;

		// Bauernabstaende pruefen

		if ((Spieler == 1)
		    && (FeldExistiert(Koenigslinie - 1, Koenigsreihe + 1))			// oben links vom weissen Koenig?
		    && (FeldSpieler[Koenigslinie - 1][Koenigsreihe + 1] == 2)
		    && (FeldSteinTyp[Koenigslinie - 1][Koenigsreihe + 1] == 1))
		return true;
		if ((Spieler == 1)
		    && (FeldExistiert(Koenigslinie + 1, Koenigsreihe + 1))			// oben rechts vom weissen Koenig?
		    && (FeldSpieler[Koenigslinie + 1][Koenigsreihe + 1] == 2)
		    && (FeldSteinTyp[Koenigslinie + 1][Koenigsreihe + 1] == 1))
		return true;
		if ((Spieler == 2)
		    && (FeldExistiert(Koenigslinie - 1, Koenigsreihe - 1))			// unten links vom schwarzen Koenig?
		    && (FeldSpieler[Koenigslinie - 1][Koenigsreihe - 1] == 1)
		    && (FeldSteinTyp[Koenigslinie - 1][Koenigsreihe - 1] == 1))
		return true;
		if ((Spieler == 2)
		    && (FeldExistiert(Koenigslinie + 1, Koenigsreihe - 1))			// unten rechts vom schwarzen Koenig?
		    && (FeldSpieler[Koenigslinie + 1][Koenigsreihe - 1] == 1)
		    && (FeldSteinTyp[Koenigslinie + 1][Koenigsreihe - 1] == 1))
		return true;

		// Geraden pruefen

		for (int i = 1; i < 8; i++)							// rechts vom Koenig
		{
			if ((!(FeldExistiert(Koenigslinie + i, Koenigsreihe))) || (FeldSpieler[Koenigslinie + i][Koenigsreihe] == Spieler)) break;
			if (FeldSpieler[Koenigslinie + i][Koenigsreihe] == (3 - Spieler))
			{
				if ((FeldSteinTyp[Koenigslinie + i][Koenigsreihe] == 4) || (FeldSteinTyp[Koenigslinie + i][Koenigsreihe] == 5))
				{
					return true;
				}
				else break;
			}
		}
		for (int i = 1; i < 8; i++)							// unter dem Koenig
		{
			if ((!(FeldExistiert(Koenigslinie, Koenigsreihe - i))) || (FeldSpieler[Koenigslinie][Koenigsreihe - i] == Spieler)) break;
			if (FeldSpieler[Koenigslinie][Koenigsreihe - i] == (3 - Spieler))
			{
				if ((FeldSteinTyp[Koenigslinie][Koenigsreihe - i] == 4) || (FeldSteinTyp[Koenigslinie][Koenigsreihe - i] == 5))
				{
					return true;
				}
				else break;
			}
		}
		for (int i = 1; i < 8; i++)							// links vom Koenig
		{
			if ((!(FeldExistiert(Koenigslinie - i, Koenigsreihe))) || (FeldSpieler[Koenigslinie - i][Koenigsreihe] == Spieler)) break;
			if (FeldSpieler[Koenigslinie - i][Koenigsreihe] == (3 - Spieler))
			{
				if ((FeldSteinTyp[Koenigslinie - i][Koenigsreihe] == 4) || (FeldSteinTyp[Koenigslinie - i][Koenigsreihe] == 5))
				{
					return true;
				}
				else break;
			}
		}
		for (int i = 1; i < 8; i++)							// ueber dem Koenig
		{
			if ((!(FeldExistiert(Koenigslinie, Koenigsreihe + i))) || (FeldSpieler[Koenigslinie][Koenigsreihe + i] == Spieler)) break;
			if (FeldSpieler[Koenigslinie][Koenigsreihe + i] == (3 - Spieler))
			{
				if ((FeldSteinTyp[Koenigslinie][Koenigsreihe + i] == 4) || (FeldSteinTyp[Koenigslinie][Koenigsreihe + i] == 5))
				{
					return true;
				}
				else break;
			}
		}

		// Diagonalen pruefen

		for (int i = 1; i < 8; i++)							// unten rechts vom Koenig
		{
			if ((!(FeldExistiert(Koenigslinie + i, Koenigsreihe - i))) || (FeldSpieler[Koenigslinie + i][Koenigsreihe - i] == Spieler))
			{
				break;
			}
			if (FeldSpieler[Koenigslinie + i][Koenigsreihe - i] == (3 - Spieler))
			{
				if ((FeldSteinTyp[Koenigslinie + i][Koenigsreihe - i] == 3)
				    || (FeldSteinTyp[Koenigslinie + i][Koenigsreihe - i] == 5))
				{
					return true;
				}
				else break;
			}
		}
		for (int i = 1; i < 8; i++)							// unten links vom Koenig
		{
			if ((!(FeldExistiert(Koenigslinie - i, Koenigsreihe - i))) || (FeldSpieler[Koenigslinie - i][Koenigsreihe - i] == Spieler))
			{
				break;
			}
			if (FeldSpieler[Koenigslinie - i][Koenigsreihe - i] == (3 - Spieler))
			{
				if ((FeldSteinTyp[Koenigslinie - i][Koenigsreihe - i] == 3)
				      || (FeldSteinTyp[Koenigslinie - i][Koenigsreihe - i] == 5))
				{
					return true;
				}
				else break;
			}
		}
		for (int i = 1; i < 8; i++)							// oben links vom Koenig
		{
			if ((!(FeldExistiert(Koenigslinie - i, Koenigsreihe + i))) || (FeldSpieler[Koenigslinie - i][Koenigsreihe + i] == Spieler))
			{
				break;
			}
			if (FeldSpieler[Koenigslinie - i][Koenigsreihe + i] == (3 - Spieler))
			{
				if ((FeldSteinTyp[Koenigslinie - i][Koenigsreihe + i] == 3)
				      || (FeldSteinTyp[Koenigslinie - i][Koenigsreihe + i] == 5))
				{
					return true;
				}
				else break;
			}
		}
		for (int i = 1; i < 8; i++)							// oben rechts vom Koenig
		{
			if ((!(FeldExistiert(Koenigslinie + i, Koenigsreihe + i))) || (FeldSpieler[Koenigslinie + i][Koenigsreihe + i] == Spieler))
			{
				break;
			}
			if (FeldSpieler[Koenigslinie + i][Koenigsreihe + i] == (3 - Spieler))
			{
				if ((FeldSteinTyp[Koenigslinie + i][Koenigsreihe + i] == 3)
				      || (FeldSteinTyp[Koenigslinie + i][Koenigsreihe + i] == 5))
				{
					return true;
				}
				else break;
			}
		}

		// Springerabstaende pruefen

		if ((FeldExistiert(Koenigslinie + 2, Koenigsreihe - 1))				// Erster Springerabstand unten rechts von Koenig
		    && (FeldSpieler[Koenigslinie + 2][Koenigsreihe - 1] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie + 2][Koenigsreihe - 1] == 2))
		{
			return true;
		}
		if ((FeldExistiert(Koenigslinie + 1, Koenigsreihe - 2))				// Zweiter Springerabstand unten rechts von Koenig
		    && (FeldSpieler[Koenigslinie + 1][Koenigsreihe - 2] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie + 1][Koenigsreihe - 2] == 2))
		{
			return true;
		}
		if ((FeldExistiert(Koenigslinie - 1, Koenigsreihe - 2))				// Erster Springerabstand unten links von Koenig
		    && (FeldSpieler[Koenigslinie - 1][Koenigsreihe - 2] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie - 1][Koenigsreihe - 2] == 2))
		{
			return true;
		}
		if ((FeldExistiert(Koenigslinie - 2, Koenigsreihe - 1))				// Zweiter Springerabstand unten links von Koenig
		    && (FeldSpieler[Koenigslinie - 2][Koenigsreihe - 1] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie - 2][Koenigsreihe - 1] == 2))
		{
			return true;
		}
		if ((FeldExistiert(Koenigslinie - 2, Koenigsreihe + 1))				// Erster Springerabstand oben links von Koenig
		    && (FeldSpieler[Koenigslinie - 2][Koenigsreihe + 1] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie - 2][Koenigsreihe + 1] == 2))
		{
			return true;
		}
		if ((FeldExistiert(Koenigslinie - 1, Koenigsreihe + 2))				// Zweiter Springerabstand oben links von Koenig
		    && (FeldSpieler[Koenigslinie - 1][Koenigsreihe + 2] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie - 1][Koenigsreihe + 2] == 2))
		{
			return true;
		}
		if ((FeldExistiert(Koenigslinie + 1, Koenigsreihe + 2))				// Erster Springerabstand oben rechts von Koenig
		    && (FeldSpieler[Koenigslinie + 1][Koenigsreihe + 2] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie + 1][Koenigsreihe + 2] == 2))
		{
			return true;
		}
		if ((FeldExistiert(Koenigslinie + 2, Koenigsreihe + 1))				// Zweiter Springerabstand oben rechts von Koenig
		    && (FeldSpieler[Koenigslinie + 2][Koenigsreihe + 1] == (3 - Spieler))
		    && (FeldSteinTyp[Koenigslinie + 2][Koenigsreihe + 1] == 2))
		{
			return true;
		}

		return false;
	}

	int Zugmoeglichkeiten(int[][] FeldSpieler, int[][] FeldSteinTyp, int[][] FeldStein,
			      int[][] SteinLinie, int[][] SteinReihe, int[][] SteinTyp,
			      int[] EnPassantLinie, boolean[] KRNMFW, boolean[] GRNMFW, boolean[] KRNMFS, boolean[] GRNMFS,
			      int[][] ZugTyp, int [][] ZugSteinTyp, int [][] ZugUmwandlungssteintyp,
			      int[][] ZugGeschlagenerSteinTyp, int[][] ZugGeschlagenerStein, int[][] ZugStein,
			      int[][] ZugStartfeldLinie, int[][] ZugStartfeldReihe, int[][] ZugZielfeldLinie, int[][] ZugZielfeldReihe,
			      int Spieler, int Tiefe)
	{
		int ZM = 0; 			// ZM bedeutet "Zugmoeglichkeiten"

		int TempLinie;			// Temp bedeutet "temporaer"
		int TempReihe;

		int TempGeschlagenerSpieler;	// Temp bedeutet "temporaer"
		int TempGeschlagenerSteinTyp;

		for (int Stein = 1; Stein < 17; Stein++)
		{
			TempLinie = SteinLinie[Spieler][Stein];
			TempReihe = SteinReihe[Spieler][Stein];

			/*
				int[][] ZugTyp:

				1. Bit: Schlagen? 				0 = nein, 1 = ja
				2. Bit: En passant?				0 = nein, 1 = ja
				3. Bit: Umwandlung?				0 = nein, 1 = ja
				4. Bit: Schach?					0 = nein, 1 = ja
				5. Bit: Rochade?				0 = nein, 1 = ja
				6. Bit: Falls Rochade, dann welche Groesse?	0 = klein, 1 = gross
			*/

			if (SteinTyp[Spieler][Stein] == 0)	// Stein wurde bereits geschlagen
			{
				continue;
			}

			if (SteinTyp[Spieler][Stein] == 1)	// Bauer
			{
				if (Spieler == 1)		// Betrachtung fuer weiss
				{
					if (FeldSpieler[TempLinie][TempReihe + 1] == 0)
					{
						// Feld vor dem Bauern ist frei

						FeldSpieler[TempLinie][TempReihe] 	= 0;	// Zug simulieren
						FeldSteinTyp[TempLinie][TempReihe] 	= 0;
						FeldSpieler[TempLinie][TempReihe + 1]	= 1;
						FeldSteinTyp[TempLinie][TempReihe + 1]	= 1;

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1], 1) == false)
						{
							// Zug waere gueltig

							if (TempReihe == 7)
							{
								// Bauer koennte umgewandelt werden

								FeldSteinTyp[TempLinie][8]	= 2;	// Umwandlung in Springer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0c;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x04;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 2;
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;

								FeldSteinTyp[TempLinie][8]	= 3;	// Umwandlung in Laeufer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0c;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x04;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 3;
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;

								FeldSteinTyp[TempLinie][8]	= 4;	// Umwandlung in Turm

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0c;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x04;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 4;
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;

								FeldSteinTyp[TempLinie][8]	= 5;	// Umwandlung in Dame

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0c;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x04;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 5;
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;
							}
							else
							{
								// Normaler Bauernvorzug

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x08;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x00;
								}

								ZugStein[Tiefe][ZM]		= Stein;
								ZugSteinTyp[Tiefe][ZM]		= 1;
								ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
								ZugZielfeldLinie[Tiefe][ZM]	= TempLinie;
								ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + 1;

								ZM += 1;
							}
						}

						FeldSpieler[TempLinie][TempReihe] 	= 1;	// Simulation beenden
						FeldSteinTyp[TempLinie][TempReihe] 	= 1;
						FeldSpieler[TempLinie][TempReihe + 1]	= 0;
						FeldSteinTyp[TempLinie][TempReihe + 1]	= 0;
					}

					if ((TempReihe == 2) && (FeldSpieler[TempLinie][3] == 0) && (FeldSpieler[TempLinie][4] == 0))
					{
						// Beide Felder vor dem Bauern auf Reihe 2 sind frei

						FeldSpieler[TempLinie][2] 	= 0;	// Zug simulieren
						FeldSteinTyp[TempLinie][2] 	= 0;
						FeldSpieler[TempLinie][4]	= 1;
						FeldSteinTyp[TempLinie][4]	= 1;

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1], 1) == false)
						{
							// Zug waere gueltig

							if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
									      2))
							{
								ZugTyp[Tiefe][ZM] = 0x08;
							}
							else
							{
								ZugTyp[Tiefe][ZM] = 0x00;
							}

							ZugStein[Tiefe][ZM]		= Stein;
							ZugSteinTyp[Tiefe][ZM]		= 1;
							ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
							ZugStartfeldReihe[Tiefe][ZM]	= 2;
							ZugZielfeldLinie[Tiefe][ZM]	= TempLinie;
							ZugZielfeldReihe[Tiefe][ZM]	= 4;

							ZM += 1;
						}

						FeldSpieler[TempLinie][2] 	= 1;	// Simulation beenden
						FeldSteinTyp[TempLinie][2] 	= 1;
						FeldSpieler[TempLinie][4]	= 0;
						FeldSteinTyp[TempLinie][4]	= 0;
					}

					if ((TempLinie > 1) && (FeldSpieler[TempLinie - 1][TempReihe + 1] == 2))
					{
						// Feld oben links vom Bauern ist durch Gegner besetzt

						FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
						FeldSteinTyp[TempLinie][TempReihe] 		= 0;
						TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - 1][TempReihe + 1];
						FeldSpieler[TempLinie - 1][TempReihe + 1]	= 1;
						FeldSteinTyp[TempLinie - 1][TempReihe + 1]	= 1;

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1], 1) == false)
						{
							// Zug waere gueltig

							if (TempReihe == 7)
							{
								// Bauer koennte umgewandelt werden

								FeldSteinTyp[TempLinie - 1][8]	= 2;	// Umwandlung in Springer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 2;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][8];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie - 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;

								FeldSteinTyp[TempLinie - 1][8]	= 3;	// Umwandlung in Laeufer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 3;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][8];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie - 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;

								FeldSteinTyp[TempLinie - 1][8]	= 4;	// Umwandlung in Turm

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 4;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][8];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie - 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;

								FeldSteinTyp[TempLinie - 1][8]	= 5;	// Umwandlung in Dame

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 5;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][8];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie - 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;
							}
							else
							{
								// Normaler schlagender Bauernzug nach oben links moeglich

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x09;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x01;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][TempReihe + 1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= TempReihe;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie - 1;
								ZugZielfeldReihe[Tiefe][ZM]		= TempReihe + 1;

								ZM += 1;
							}
						}

						FeldSpieler[TempLinie][TempReihe] 		= 1;	// Simulation beenden
						FeldSteinTyp[TempLinie][TempReihe] 		= 1;
						FeldSpieler[TempLinie - 1][TempReihe + 1]	= 2;
						FeldSteinTyp[TempLinie - 1][TempReihe + 1]	= TempGeschlagenerSteinTyp;
					}

					if ((TempLinie < 8) && (FeldSpieler[TempLinie + 1][TempReihe + 1] == 2))
					{
						// Feld oben rechts vom Bauern ist durch Gegner besetzt

						FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
						FeldSteinTyp[TempLinie][TempReihe] 		= 0;
						TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + 1][TempReihe + 1];
						FeldSpieler[TempLinie + 1][TempReihe + 1]	= 1;
						FeldSteinTyp[TempLinie + 1][TempReihe + 1]	= 1;

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1], 1) == false)
						{
							// Zug waere gueltig

							if (TempReihe == 7)
							{
								// Bauer koennte umgewandelt werden

								FeldSteinTyp[TempLinie + 1][8]	= 2;	// Umwandlung in Springer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 2;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][8];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie + 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;

								FeldSteinTyp[TempLinie + 1][8]	= 3;	// Umwandlung in Laeufer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 3;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][8];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie + 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;

								FeldSteinTyp[TempLinie + 1][8]	= 4;	// Umwandlung in Turm

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 4;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][8];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie + 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;

								FeldSteinTyp[TempLinie + 1][8]	= 5;	// Umwandlung in Dame

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 5;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][8];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 7;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie + 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 8;

								ZM += 1;
							}
							else
							{
								// Normaler schlagender Bauernzug nach oben rechts moeglich

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
										      2))
								{
									ZugTyp[Tiefe][ZM] = 0x09;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x01;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][TempReihe + 1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= TempReihe;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie + 1;
								ZugZielfeldReihe[Tiefe][ZM]		= TempReihe + 1;

								ZM += 1;
							}
						}

						FeldSpieler[TempLinie][TempReihe] 		= 1;	// Simulation beenden
						FeldSteinTyp[TempLinie][TempReihe] 		= 1;
						FeldSpieler[TempLinie + 1][TempReihe + 1]	= 2;
						FeldSteinTyp[TempLinie + 1][TempReihe + 1]	= TempGeschlagenerSteinTyp;
					}

					if ((EnPassantLinie[0] != 0)
					    && (TempReihe == 5) && ((EnPassantLinie[0] - TempLinie == -1) || (EnPassantLinie[0] - TempLinie == 1)))
					{
						// Neben diesem Bauern zog zuvor ein Bauer zwei Zuege entgegen

						FeldSpieler[TempLinie][5] 		= 0;	// Zug simulieren
						FeldSteinTyp[TempLinie][5] 		= 0;
						FeldSpieler[EnPassantLinie[0]][6]	= 1;
						FeldSteinTyp[EnPassantLinie[0]][6]	= 1;
						FeldSpieler[EnPassantLinie[0]][5] 	= 0;
						FeldSteinTyp[EnPassantLinie[0]][5] 	= 0;

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1], 1) == false)
						{
							// Zug waere gueltig

							if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1],
									      2))
							{
								ZugTyp[Tiefe][ZM] = 0x0b;
							}
							else
							{
								ZugTyp[Tiefe][ZM] = 0x03;
							}

							ZugStein[Tiefe][ZM]			= Stein;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= 1;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[EnPassantLinie[0]][5];
							ZugSteinTyp[Tiefe][ZM]			= 1;
							ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
							ZugStartfeldReihe[Tiefe][ZM]		= 5;
							ZugZielfeldLinie[Tiefe][ZM]		= EnPassantLinie[0];
							ZugZielfeldReihe[Tiefe][ZM]		= 6;

							ZM += 1;
						}

						FeldSpieler[TempLinie][5] 		= 1;	// Simulation beenden
						FeldSteinTyp[TempLinie][5] 		= 1;
						FeldSpieler[EnPassantLinie[0]][6]	= 0;
						FeldSteinTyp[EnPassantLinie[0]][6]	= 0;
						FeldSpieler[EnPassantLinie[0]][5] 	= 2;
						FeldSteinTyp[EnPassantLinie[0]][5] 	= 1;
					}	
				}
				else			// Betrachtung fuer schwarz
				{
					if (FeldSpieler[TempLinie][TempReihe - 1] == 0)
					{
						// Feld vor dem Bauern ist frei

						FeldSpieler[TempLinie][TempReihe] 	= 0;	// Zug simulieren
						FeldSteinTyp[TempLinie][TempReihe] 	= 0;
						FeldSpieler[TempLinie][TempReihe - 1]	= 2;
						FeldSteinTyp[TempLinie][TempReihe - 1]	= 1;

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1], 2) == false)
						{
							// Zug waere gueltig

							if (TempReihe == 2)
							{
								// Bauer koennte umgewandelt werden

								FeldSteinTyp[TempLinie][1]	= 2;	// Umwandlung in Springer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0c;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x04;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 2;
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;

								ZM += 1;

								FeldSteinTyp[TempLinie][1]	= 3;	// Umwandlung in Laeufer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0c;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x04;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 3;
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;

								ZM += 1;

								FeldSteinTyp[TempLinie][1]	= 4;	// Umwandlung in Turm

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0c;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x04;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 4;
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;

								ZM += 1;

								FeldSteinTyp[TempLinie][1]	= 5;	// Umwandlung in Dame

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0c;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x04;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 5;
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;

								ZM += 1;
							}
							else
							{
								// Normaler Bauernvorzug

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x08;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x00;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugSteinTyp[Tiefe][ZM]		= 1;
								ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
								ZugZielfeldLinie[Tiefe][ZM]	= TempLinie;
								ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - 1;

								ZM += 1;
							}
						}

						FeldSpieler[TempLinie][TempReihe] 	= 2;	// Simulation beenden
						FeldSteinTyp[TempLinie][TempReihe] 	= 1;
						FeldSpieler[TempLinie][TempReihe - 1]	= 0;
						FeldSteinTyp[TempLinie][TempReihe - 1]	= 0;
					}

					if ((TempReihe == 7) && (FeldSpieler[TempLinie][6] == 0) && (FeldSpieler[TempLinie][5] == 0))
					{
						// Beide Felder vor dem Bauern auf Reihe 7 sind frei

						FeldSpieler[TempLinie][7] 	= 0;	// Zug simulieren
						FeldSteinTyp[TempLinie][7] 	= 0;
						FeldSpieler[TempLinie][5]	= 2;
						FeldSteinTyp[TempLinie][5]	= 1;

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1], 2) == false)
						{
							// Zug waere gueltig

							if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
									      1))
							{
								ZugTyp[Tiefe][ZM] = 0x08;
							}
							else
							{
								ZugTyp[Tiefe][ZM] = 0x00;
							}

							ZugStein[Tiefe][ZM]		= Stein;
							ZugSteinTyp[Tiefe][ZM]		= 1;
							ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
							ZugStartfeldReihe[Tiefe][ZM]	= 7;
							ZugZielfeldLinie[Tiefe][ZM]	= TempLinie;
							ZugZielfeldReihe[Tiefe][ZM]	= 5;

							ZM += 1;
						}

						FeldSpieler[TempLinie][7] 	= 2;	// Simulation beenden
						FeldSteinTyp[TempLinie][7] 	= 1;
						FeldSpieler[TempLinie][5]	= 0;
						FeldSteinTyp[TempLinie][5]	= 0;
					}

					if ((TempLinie > 1) && (FeldSpieler[TempLinie - 1][TempReihe - 1] == 1))
					{
						// Feld unten links vom Bauern ist durch Gegner besetzt

						FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
						FeldSteinTyp[TempLinie][TempReihe] 		= 0;
						TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - 1][TempReihe - 1];
						FeldSpieler[TempLinie - 1][TempReihe - 1]	= 2;
						FeldSteinTyp[TempLinie - 1][TempReihe - 1]	= 1;

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1], 2) == false)
						{
							// Zug waere gueltig

							if (TempReihe == 2)
							{
								// Bauer koennte umgewandelt werden

								FeldSteinTyp[TempLinie - 1][1]	= 2;	// Umwandlung in Springer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 2;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie - 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;

								ZM += 1;

								FeldSteinTyp[TempLinie - 1][1]	= 3;	// Umwandlung in Laeufer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 3;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie - 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;


								ZM += 1;

								FeldSteinTyp[TempLinie - 1][1]	= 4;	// Umwandlung in Turm

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 4;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie - 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;

								ZM += 1;

								FeldSteinTyp[TempLinie - 1][1]	= 5;	// Umwandlung in Dame

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 5;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie - 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;

								ZM += 1;
							}
							else
							{
								// Normaler schlagender Bauernzug nach unten links moeglich

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x09;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x01;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][TempReihe - 1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= TempReihe;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie - 1;
								ZugZielfeldReihe[Tiefe][ZM]		= TempReihe - 1;

								ZM += 1;
							}
						}

						FeldSpieler[TempLinie][TempReihe] 		= 2;	// Simulation beenden
						FeldSteinTyp[TempLinie][TempReihe] 		= 1;
						FeldSpieler[TempLinie - 1][TempReihe - 1]	= 1;
						FeldSteinTyp[TempLinie - 1][TempReihe - 1]	= TempGeschlagenerSteinTyp;
					}

					if ((TempLinie < 8) && (FeldSpieler[TempLinie + 1][TempReihe - 1] == 1))
					{
						// Feld unten rechts vom Bauern ist durch Gegner besetzt

						FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
						FeldSteinTyp[TempLinie][TempReihe] 		= 0;
						TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + 1][TempReihe - 1];
						FeldSpieler[TempLinie + 1][TempReihe - 1]	= 1;
						FeldSteinTyp[TempLinie + 1][TempReihe - 1]	= 1;

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1], 2) == false)
						{
							// Zug waere gueltig

							if (TempReihe == 2)
							{
								// Bauer koennte umgewandelt werden

								FeldSteinTyp[TempLinie + 1][1]	= 2;	// Umwandlung in Springer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 2;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie + 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;

								ZM += 1;

								FeldSteinTyp[TempLinie + 1][1]	= 3;	// Umwandlung in Laeufer

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 3;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie + 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;

								ZM += 1;

								FeldSteinTyp[TempLinie + 1][1]	= 4;	// Umwandlung in Turm

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 4;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie + 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;

								ZM += 1;

								FeldSteinTyp[TempLinie + 1][1]	= 5;	// Umwandlung in Dame

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x0d;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x05;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugUmwandlungssteintyp[Tiefe][ZM]	= 5;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= 2;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie + 1;
								ZugZielfeldReihe[Tiefe][ZM]		= 1;

								ZM += 1;
							}
							else
							{
								// Normaler schlagender Bauernzug nach unten rechts moeglich

								if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
										      1))
								{
									ZugTyp[Tiefe][ZM] = 0x09;
								}
								else
								{
									ZugTyp[Tiefe][ZM] = 0x01;
								}

								ZugStein[Tiefe][ZM]			= Stein;
								ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
								ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][TempReihe - 1];
								ZugSteinTyp[Tiefe][ZM]			= 1;
								ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
								ZugStartfeldReihe[Tiefe][ZM]		= TempReihe;
								ZugZielfeldLinie[Tiefe][ZM]		= TempLinie + 1;
								ZugZielfeldReihe[Tiefe][ZM]		= TempReihe - 1;

								ZM += 1;
							}
						}

						FeldSpieler[TempLinie][TempReihe] 		= 2;	// Simulation beenden
						FeldSteinTyp[TempLinie][TempReihe] 		= 1;
						FeldSpieler[TempLinie + 1][TempReihe - 1]	= 1;
						FeldSteinTyp[TempLinie + 1][TempReihe - 1]	= TempGeschlagenerSteinTyp;
					}

					if ((EnPassantLinie[0] != 0)
					    && (TempReihe == 4) && ((EnPassantLinie[0] - TempLinie == -1) || (EnPassantLinie[0] - TempLinie == 1)))
					{
						// Neben diesem Bauern zog zuvor ein Bauer zwei Zuege entgegen

						FeldSpieler[TempLinie][4] 		= 0;	// Zug simulieren
						FeldSteinTyp[TempLinie][4] 		= 0;
						FeldSpieler[EnPassantLinie[0]][3]	= 1;
						FeldSteinTyp[EnPassantLinie[0]][3]	= 1;
						FeldSpieler[EnPassantLinie[0]][4] 	= 0;
						FeldSteinTyp[EnPassantLinie[0]][4] 	= 0;

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[2][1], SteinReihe[2][1], 2) == false)
						{
							// Zug waere gueltig

							if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[1][1], SteinReihe[1][1],
									      1))
							{
								ZugTyp[Tiefe][ZM] = 0x0b;
							}
							else
							{
								ZugTyp[Tiefe][ZM] = 0x03;
							}

							ZugStein[Tiefe][ZM]			= Stein;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= 1;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[EnPassantLinie[0]][4];
							ZugSteinTyp[Tiefe][ZM]			= 1;
							ZugStartfeldLinie[Tiefe][ZM]		= TempLinie;
							ZugStartfeldReihe[Tiefe][ZM]		= 4;
							ZugZielfeldLinie[Tiefe][ZM]		= EnPassantLinie[0];
							ZugZielfeldReihe[Tiefe][ZM]		= 3;

							ZM += 1;
						}

						FeldSpieler[TempLinie][4] 		= 2;	// Simulation beenden
						FeldSteinTyp[TempLinie][4] 		= 1;
						FeldSpieler[EnPassantLinie[0]][3]	= 0;
						FeldSteinTyp[EnPassantLinie[0]][3]	= 0;
						FeldSpieler[EnPassantLinie[0]][4] 	= 1;
						FeldSteinTyp[EnPassantLinie[0]][4] 	= 1;
					}	
				}
			}

			if (SteinTyp[Spieler][Stein] == 2)	// Springer
			{
				if (FeldExistiert(TempLinie + 2, TempReihe - 1) && (FeldSpieler[TempLinie + 2][TempReihe - 1] != Spieler))
				{
					// Feld im ersten Springerabstand unten rechts

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + 2][TempReihe - 1];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + 2][TempReihe - 1];
					FeldSpieler[TempLinie + 2][TempReihe - 1]	= Spieler;
					FeldSteinTyp[TempLinie + 2][TempReihe - 1]	= 2;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM]			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 2][TempReihe - 1];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 2;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + 2;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - 1;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 2;
					FeldSpieler[TempLinie + 2][TempReihe - 1] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + 2][TempReihe - 1] 	= TempGeschlagenerSteinTyp;
				}

				if (FeldExistiert(TempLinie + 1, TempReihe - 2) && (FeldSpieler[TempLinie + 1][TempReihe - 2] != Spieler))
				{
					// Feld im zweiten Springerabstand unten rechts

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + 1][TempReihe - 2];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + 1][TempReihe - 2];
					FeldSpieler[TempLinie + 1][TempReihe - 2]	= Spieler;
					FeldSteinTyp[TempLinie + 1][TempReihe - 2]	= 2;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM]			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][TempReihe - 2];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 2;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + 1;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - 2;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 2;
					FeldSpieler[TempLinie + 1][TempReihe - 2] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + 1][TempReihe - 2] 	= TempGeschlagenerSteinTyp;
				}

				if (FeldExistiert(TempLinie - 1, TempReihe - 2) && (FeldSpieler[TempLinie - 1][TempReihe - 2] != Spieler))
				{
					// Feld im ersten Springerabstand unten links

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - 1][TempReihe - 2];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - 1][TempReihe - 2];
					FeldSpieler[TempLinie - 1][TempReihe - 2]	= Spieler;
					FeldSteinTyp[TempLinie - 1][TempReihe - 2]	= 2;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM]			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][TempReihe - 2];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 2;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - 1;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - 2;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 2;
					FeldSpieler[TempLinie - 1][TempReihe - 2] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - 1][TempReihe - 2] 	= TempGeschlagenerSteinTyp;
				}

				if (FeldExistiert(TempLinie - 2, TempReihe - 1) && (FeldSpieler[TempLinie - 2][TempReihe - 1] != Spieler))
				{
					// Feld im zweiten Springerabstand unten links

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - 2][TempReihe - 1];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - 2][TempReihe - 1];
					FeldSpieler[TempLinie - 2][TempReihe - 1]	= Spieler;
					FeldSteinTyp[TempLinie - 2][TempReihe - 1]	= 2;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM]			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 2][TempReihe - 1];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 2;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - 2;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - 1;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 2;
					FeldSpieler[TempLinie - 2][TempReihe - 1] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - 2][TempReihe - 1] 	= TempGeschlagenerSteinTyp;
				}

				if (FeldExistiert(TempLinie - 2, TempReihe + 1) && (FeldSpieler[TempLinie - 2][TempReihe + 1] != Spieler))
				{
					// Feld im ersten Springerabstand oben links

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - 2][TempReihe + 1];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - 2][TempReihe + 1];
					FeldSpieler[TempLinie - 2][TempReihe + 1]	= Spieler;
					FeldSteinTyp[TempLinie - 2][TempReihe + 1]	= 2;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM]			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 2][TempReihe + 1];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 2;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - 2;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + 1;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 2;
					FeldSpieler[TempLinie - 2][TempReihe + 1] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - 2][TempReihe + 1] 	= TempGeschlagenerSteinTyp;
				}

				if (FeldExistiert(TempLinie - 1, TempReihe + 2) && (FeldSpieler[TempLinie - 1][TempReihe + 2] != Spieler))
				{
					// Feld im zweiten Springerabstand oben links

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - 1][TempReihe + 2];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - 1][TempReihe + 2];
					FeldSpieler[TempLinie - 1][TempReihe + 2]	= Spieler;
					FeldSteinTyp[TempLinie - 1][TempReihe + 2]	= 2;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM]			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][TempReihe + 2];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 2;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - 1;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + 2;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 2;
					FeldSpieler[TempLinie - 1][TempReihe + 2] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - 1][TempReihe + 2] 	= TempGeschlagenerSteinTyp;
				}

				if (FeldExistiert(TempLinie + 1, TempReihe + 2) && (FeldSpieler[TempLinie + 1][TempReihe + 2] != Spieler))
				{
					// Feld im ersten Springerabstand oben rechts

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + 1][TempReihe + 2];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + 1][TempReihe + 2];
					FeldSpieler[TempLinie + 1][TempReihe + 2]	= Spieler;
					FeldSteinTyp[TempLinie + 1][TempReihe + 2]	= 2;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM]			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][TempReihe + 2];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 2;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + 1;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + 2;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 2;
					FeldSpieler[TempLinie + 1][TempReihe + 2] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + 1][TempReihe + 2] 	= TempGeschlagenerSteinTyp;
				}

				if (FeldExistiert(TempLinie + 2, TempReihe + 1) && (FeldSpieler[TempLinie + 2][TempReihe + 1] != Spieler))
				{
					// Feld im zweiten Springerabstand oben rechts

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + 2][TempReihe + 1];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + 2][TempReihe + 1];
					FeldSpieler[TempLinie + 2][TempReihe + 1]	= Spieler;
					FeldSteinTyp[TempLinie + 2][TempReihe + 1]	= 2;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM]			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 2][TempReihe + 1];
						};

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 2;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + 2;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + 1;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 2;
					FeldSpieler[TempLinie + 2][TempReihe + 1] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + 2][TempReihe + 1] 	= TempGeschlagenerSteinTyp;
				}
			}

			if (SteinTyp[Spieler][Stein] == 3)	// Laeufer
			{
				for (int i = 1; i < 8; i++)					// Diagonale unten rechts
				{
					if ((!(FeldExistiert(TempLinie + i, TempReihe - i)))
					    || (FeldSpieler[TempLinie + i][TempReihe - i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + i][TempReihe - i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + i][TempReihe - i];
					FeldSpieler[TempLinie + i][TempReihe - i]	= Spieler;
					FeldSteinTyp[TempLinie + i][TempReihe - i]	= 3;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + i][TempReihe - i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 3;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 3;
					FeldSpieler[TempLinie + i][TempReihe - i] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + i][TempReihe - i] 	= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Diagonale unten links
				{
					if ((!(FeldExistiert(TempLinie - i, TempReihe - i)))
					    || (FeldSpieler[TempLinie - i][TempReihe - i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - i][TempReihe - i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - i][TempReihe - i];
					FeldSpieler[TempLinie - i][TempReihe - i]	= Spieler;
					FeldSteinTyp[TempLinie - i][TempReihe - i]	= 3;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - i][TempReihe - i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 3;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 3;
					FeldSpieler[TempLinie - i][TempReihe - i] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - i][TempReihe - i] 	= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Diagonale oben links
				{
					if ((!(FeldExistiert(TempLinie - i, TempReihe + i)))
					    || (FeldSpieler[TempLinie - i][TempReihe + i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - i][TempReihe + i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - i][TempReihe + i];
					FeldSpieler[TempLinie - i][TempReihe + i]	= Spieler;
					FeldSteinTyp[TempLinie - i][TempReihe + i]	= 3;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - i][TempReihe + i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 3;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 3;
					FeldSpieler[TempLinie - i][TempReihe + i] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - i][TempReihe + i] 	= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Diagonale oben rechts
				{
					if ((!(FeldExistiert(TempLinie + i, TempReihe + i)))
					    || (FeldSpieler[TempLinie + i][TempReihe + i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + i][TempReihe + i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + i][TempReihe + i];
					FeldSpieler[TempLinie + i][TempReihe + i]	= Spieler;
					FeldSteinTyp[TempLinie + i][TempReihe + i]	= 3;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + i][TempReihe + i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 3;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 3;
					FeldSpieler[TempLinie + i][TempReihe + i] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + i][TempReihe + i] 	= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}
			}

			if (SteinTyp[Spieler][Stein] == 4)	// Turm
			{
				for (int i = 1; i < 8; i++)					// Gerade rechts
				{
					if ((!(FeldExistiert(TempLinie + i, TempReihe)))
					    || (FeldSpieler[TempLinie + i][TempReihe] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + i][TempReihe];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + i][TempReihe];
					FeldSpieler[TempLinie + i][TempReihe]		= Spieler;
					FeldSteinTyp[TempLinie + i][TempReihe]		= 4;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + i][TempReihe];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 4;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 4;
					FeldSpieler[TempLinie + i][TempReihe] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + i][TempReihe] 		= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Gerade unten
				{
					if ((!(FeldExistiert(TempLinie, TempReihe - i)))
					    || (FeldSpieler[TempLinie][TempReihe - i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie][TempReihe - i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie][TempReihe - i];
					FeldSpieler[TempLinie][TempReihe - i]		= Spieler;
					FeldSteinTyp[TempLinie][TempReihe - i]		= 4;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie][TempReihe - i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 4;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 4;
					FeldSpieler[TempLinie][TempReihe - i] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie][TempReihe - i] 		= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Gerade links
				{
					if ((!(FeldExistiert(TempLinie - i, TempReihe)))
					    || (FeldSpieler[TempLinie - i][TempReihe] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - i][TempReihe];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - i][TempReihe];
					FeldSpieler[TempLinie - i][TempReihe]		= Spieler;
					FeldSteinTyp[TempLinie - i][TempReihe]		= 4;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - i][TempReihe];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 4;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 4;
					FeldSpieler[TempLinie - i][TempReihe] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - i][TempReihe] 		= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Gerade oben
				{
					if ((!(FeldExistiert(TempLinie, TempReihe + i)))
					    || (FeldSpieler[TempLinie][TempReihe + i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie][TempReihe + i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie][TempReihe + i];
					FeldSpieler[TempLinie][TempReihe + i]		= Spieler;
					FeldSteinTyp[TempLinie][TempReihe + i]		= 4;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie][TempReihe + i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 4;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 4;
					FeldSpieler[TempLinie][TempReihe + i] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie][TempReihe + i] 		= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}
			}

			if (SteinTyp[Spieler][Stein] == 5)	// Dame
			{
				for (int i = 1; i < 8; i++)					// Gerade rechts
				{
					if ((!(FeldExistiert(TempLinie + i, TempReihe)))
					    || (FeldSpieler[TempLinie + i][TempReihe] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + i][TempReihe];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + i][TempReihe];
					FeldSpieler[TempLinie + i][TempReihe]		= Spieler;
					FeldSteinTyp[TempLinie + i][TempReihe]		= 5;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + i][TempReihe];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 5;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 5;
					FeldSpieler[TempLinie + i][TempReihe] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + i][TempReihe] 		= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Diagonale unten rechts
				{
					if ((!(FeldExistiert(TempLinie + i, TempReihe - i)))
					    || (FeldSpieler[TempLinie + i][TempReihe - i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + i][TempReihe - i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + i][TempReihe - i];
					FeldSpieler[TempLinie + i][TempReihe - i]	= Spieler;
					FeldSteinTyp[TempLinie + i][TempReihe - i]	= 5;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + i][TempReihe - i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 5;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 5;
					FeldSpieler[TempLinie + i][TempReihe - i] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + i][TempReihe - i] 	= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Gerade unten
				{
					if ((!(FeldExistiert(TempLinie, TempReihe - i)))
					    || (FeldSpieler[TempLinie][TempReihe - i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie][TempReihe - i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie][TempReihe - i];
					FeldSpieler[TempLinie][TempReihe - i]		= Spieler;
					FeldSteinTyp[TempLinie][TempReihe - i]		= 5;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie][TempReihe - i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 5;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 5;
					FeldSpieler[TempLinie][TempReihe - i] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie][TempReihe - i] 		= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Diagonale unten links
				{
					if ((!(FeldExistiert(TempLinie - i, TempReihe - i)))
					    || (FeldSpieler[TempLinie - i][TempReihe - i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - i][TempReihe - i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - i][TempReihe - i];
					FeldSpieler[TempLinie - i][TempReihe - i]	= Spieler;
					FeldSteinTyp[TempLinie - i][TempReihe - i]	= 5;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - i][TempReihe - i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 5;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 5;
					FeldSpieler[TempLinie - i][TempReihe - i] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - i][TempReihe - i] 	= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Gerade links
				{
					if ((!(FeldExistiert(TempLinie - i, TempReihe)))
					    || (FeldSpieler[TempLinie - i][TempReihe] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - i][TempReihe];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - i][TempReihe];
					FeldSpieler[TempLinie - i][TempReihe]		= Spieler;
					FeldSteinTyp[TempLinie - i][TempReihe]		= 5;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - i][TempReihe];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 5;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 5;
					FeldSpieler[TempLinie - i][TempReihe] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - i][TempReihe] 		= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Diagonale oben links
				{
					if ((!(FeldExistiert(TempLinie - i, TempReihe + i)))
					    || (FeldSpieler[TempLinie - i][TempReihe + i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - i][TempReihe + i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - i][TempReihe + i];
					FeldSpieler[TempLinie - i][TempReihe + i]	= Spieler;
					FeldSteinTyp[TempLinie - i][TempReihe + i]	= 5;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - i][TempReihe + i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 5;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 5;
					FeldSpieler[TempLinie - i][TempReihe + i] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - i][TempReihe + i] 	= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Gerade oben
				{
					if ((!(FeldExistiert(TempLinie, TempReihe + i)))
					    || (FeldSpieler[TempLinie][TempReihe + i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie][TempReihe + i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie][TempReihe + i];
					FeldSpieler[TempLinie][TempReihe + i]		= Spieler;
					FeldSteinTyp[TempLinie][TempReihe + i]		= 5;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie][TempReihe + i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 5;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 5;
					FeldSpieler[TempLinie][TempReihe + i] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie][TempReihe + i] 		= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}

				for (int i = 1; i < 8; i++)					// Diagonale oben rechts
				{
					if ((!(FeldExistiert(TempLinie + i, TempReihe + i)))
					    || (FeldSpieler[TempLinie + i][TempReihe + i] == Spieler))
					{
						break;
					}

					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + i][TempReihe + i];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + i][TempReihe + i];
					FeldSpieler[TempLinie + i][TempReihe + i]	= Spieler;
					FeldSteinTyp[TempLinie + i][TempReihe + i]	= 5;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler)
					    == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								      SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + i][TempReihe + i];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 5;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + i;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + i;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 5;
					FeldSpieler[TempLinie + i][TempReihe + i] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + i][TempReihe + i] 	= TempGeschlagenerSteinTyp;

					if (TempGeschlagenerSpieler != 0) break;
				}
			}

			if (SteinTyp[Spieler][Stein] == 6)	// Koenig
			{
				if (FeldExistiert(TempLinie + 1, TempReihe)			// Feld rechts
				    && (FeldSpieler[TempLinie + 1][TempReihe] != Spieler))
				{
					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + 1][TempReihe];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + 1][TempReihe];
					FeldSpieler[TempLinie + 1][TempReihe]		= Spieler;
					FeldSteinTyp[TempLinie + 1][TempReihe]		= 6;
					SteinLinie[Spieler][1]				= TempLinie + 1;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, TempLinie + 1, TempReihe, Spieler) == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][TempReihe];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 6;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + 1;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 6;
					FeldSpieler[TempLinie + 1][TempReihe] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + 1][TempReihe] 		= TempGeschlagenerSteinTyp;
					SteinLinie[Spieler][1]				= TempLinie;
				}

				if (FeldExistiert(TempLinie + 1, TempReihe - 1)			// Feld unten rechts
				    && (FeldSpieler[TempLinie + 1][TempReihe - 1] != Spieler))
				{
					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + 1][TempReihe - 1];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + 1][TempReihe - 1];
					FeldSpieler[TempLinie + 1][TempReihe - 1]	= Spieler;
					FeldSteinTyp[TempLinie + 1][TempReihe - 1]	= 6;
					SteinLinie[Spieler][1]				= TempLinie + 1;
					SteinReihe[Spieler][1]				= TempReihe - 1;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, TempLinie + 1, TempReihe - 1, Spieler) == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][TempReihe - 1];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 6;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + 1;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - 1;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 6;
					FeldSpieler[TempLinie + 1][TempReihe - 1] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + 1][TempReihe - 1] 	= TempGeschlagenerSteinTyp;
					SteinLinie[Spieler][1]				= TempLinie;
					SteinReihe[Spieler][1]				= TempReihe;
				}

				if (FeldExistiert(TempLinie, TempReihe - 1)			// Feld unten
				    && (FeldSpieler[TempLinie][TempReihe - 1] != Spieler))
				{
					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie][TempReihe - 1];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie][TempReihe - 1];
					FeldSpieler[TempLinie][TempReihe - 1]		= Spieler;
					FeldSteinTyp[TempLinie][TempReihe - 1]		= 6;
					SteinReihe[Spieler][1]				= TempReihe - 1;
	
					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, TempLinie, TempReihe - 1, Spieler) == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie][TempReihe - 1];
						};

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 6;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - 1;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 6;
					FeldSpieler[TempLinie][TempReihe - 1] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie][TempReihe - 1] 		= TempGeschlagenerSteinTyp;
					SteinReihe[Spieler][1]				= TempReihe;
				}

				if (FeldExistiert(TempLinie - 1, TempReihe - 1)			// Feld unten links
				    && (FeldSpieler[TempLinie - 1][TempReihe - 1] != Spieler))
				{
					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - 1][TempReihe - 1];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - 1][TempReihe - 1];
					FeldSpieler[TempLinie - 1][TempReihe - 1]	= Spieler;
					FeldSteinTyp[TempLinie - 1][TempReihe - 1]	= 6;
					SteinLinie[Spieler][1]				= TempLinie - 1;
					SteinReihe[Spieler][1]				= TempReihe - 1;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, TempLinie - 1, TempReihe - 1, Spieler) == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][TempReihe - 1];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 6;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - 1;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe - 1;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 6;
					FeldSpieler[TempLinie - 1][TempReihe - 1] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - 1][TempReihe - 1] 	= TempGeschlagenerSteinTyp;
					SteinLinie[Spieler][1]				= TempLinie;
					SteinReihe[Spieler][1]				= TempReihe;
				}

				if (FeldExistiert(TempLinie - 1, TempReihe)			// Feld links
				    && (FeldSpieler[TempLinie - 1][TempReihe] != Spieler))
				{
					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - 1][TempReihe];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - 1][TempReihe];
					FeldSpieler[TempLinie - 1][TempReihe]		= Spieler;
					FeldSteinTyp[TempLinie - 1][TempReihe]		= 6;
					SteinLinie[Spieler][1]				= TempLinie - 1;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, TempLinie - 1, TempReihe, Spieler) == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][TempReihe];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 6;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - 1;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 6;
					FeldSpieler[TempLinie - 1][TempReihe] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - 1][TempReihe] 		= TempGeschlagenerSteinTyp;
					SteinLinie[Spieler][1]				= TempLinie;
				}

				if (FeldExistiert(TempLinie - 1, TempReihe + 1)			// Feld oben links
				    && (FeldSpieler[TempLinie - 1][TempReihe + 1] != Spieler))
				{
					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie - 1][TempReihe + 1];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie - 1][TempReihe + 1];
					FeldSpieler[TempLinie - 1][TempReihe + 1]	= Spieler;
					FeldSteinTyp[TempLinie - 1][TempReihe + 1]	= 6;
					SteinLinie[Spieler][1]				= TempLinie - 1;
					SteinReihe[Spieler][1]				= TempReihe + 1;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, TempLinie - 1, TempReihe + 1, Spieler) == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie - 1][TempReihe + 1];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 6;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie - 1;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + 1;

						ZM += 1;
					}
	
					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 6;
					FeldSpieler[TempLinie - 1][TempReihe + 1] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie - 1][TempReihe + 1] 	= TempGeschlagenerSteinTyp;
					SteinLinie[Spieler][1]				= TempLinie;
					SteinReihe[Spieler][1]				= TempReihe;
				}

				if (FeldExistiert(TempLinie, TempReihe + 1)			// Feld oben
				    && (FeldSpieler[TempLinie][TempReihe + 1] != Spieler))
				{
					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie][TempReihe + 1];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie][TempReihe + 1];
					FeldSpieler[TempLinie][TempReihe + 1]		= Spieler;
					FeldSteinTyp[TempLinie][TempReihe + 1]		= 6;
					SteinReihe[Spieler][1]				= TempReihe + 1;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, TempLinie, TempReihe + 1, Spieler) == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie][TempReihe + 1];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 6;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + 1;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 6;
					FeldSpieler[TempLinie][TempReihe + 1] 		= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie][TempReihe + 1] 		= TempGeschlagenerSteinTyp;
					SteinReihe[Spieler][1]				= TempReihe;
				}

				if (FeldExistiert(TempLinie + 1, TempReihe + 1)			// Feld oben rechts
				    && (FeldSpieler[TempLinie + 1][TempReihe + 1] != Spieler))
				{
					FeldSpieler[TempLinie][TempReihe] 		= 0;	// Zug simulieren
					FeldSteinTyp[TempLinie][TempReihe] 		= 0;
					TempGeschlagenerSpieler				= FeldSpieler[TempLinie + 1][TempReihe + 1];
					TempGeschlagenerSteinTyp			= FeldSteinTyp[TempLinie + 1][TempReihe + 1];
					FeldSpieler[TempLinie + 1][TempReihe + 1]	= Spieler;
					FeldSteinTyp[TempLinie + 1][TempReihe + 1]	= 6;
					SteinLinie[Spieler][1]				= TempLinie + 1;
					SteinReihe[Spieler][1]				= TempReihe + 1;

					if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, TempLinie + 1, TempReihe + 1, Spieler) == false)
					{
						// Zug waere gueltig

						if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[3 - Spieler][1],
								SteinReihe[3 - Spieler][1], 3 - Spieler))
						{
							ZugTyp[Tiefe][ZM] = 0x08;
						}
						else
						{
							ZugTyp[Tiefe][ZM] = 0x00;
						}

						if (TempGeschlagenerSpieler == 0)
						{
							ZugTyp[Tiefe][ZM] += 0x00;
						}
						else
						{
							ZugTyp[Tiefe][ZM] 			+= 0x01;
							ZugGeschlagenerSteinTyp[Tiefe][ZM]	= TempGeschlagenerSteinTyp;
							ZugGeschlagenerStein[Tiefe][ZM]		= FeldStein[TempLinie + 1][TempReihe + 1];
						}

						ZugStein[Tiefe][ZM]		= Stein;
						ZugSteinTyp[Tiefe][ZM]		= 6;
						ZugStartfeldLinie[Tiefe][ZM]	= TempLinie;
						ZugStartfeldReihe[Tiefe][ZM]	= TempReihe;
						ZugZielfeldLinie[Tiefe][ZM]	= TempLinie + 1;
						ZugZielfeldReihe[Tiefe][ZM]	= TempReihe + 1;

						ZM += 1;
					}

					FeldSpieler[TempLinie][TempReihe] 		= Spieler;	// Simulation beenden
					FeldSteinTyp[TempLinie][TempReihe] 		= 6;
					FeldSpieler[TempLinie + 1][TempReihe + 1] 	= TempGeschlagenerSpieler;
					FeldSteinTyp[TempLinie + 1][TempReihe + 1] 	= TempGeschlagenerSteinTyp;
					SteinLinie[Spieler][1]				= TempLinie;
					SteinReihe[Spieler][1]				= TempReihe;
				}
			}
		}

		if (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, SteinLinie[Spieler][1], SteinReihe[Spieler][1], Spieler) == false)
		{
			// Koenig steht nicht im Schach

			if (Spieler == 1)		// Betrachtung fuer weiss
			{
				if ((KRNMFW[Tiefe] == true) && (FeldSpieler[6][1] == 0)  && (FeldSpieler[7][1] == 0)
				    && (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, 6, 1, 1) == false)
				    && (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, 7, 1, 1) == false))
				{
					// Kleine Rochade

					ZugTyp[Tiefe][ZM] 		= 0x10;

					ZM += 1;
				}

				if ((GRNMFW[Tiefe] == true) && (FeldSpieler[4][1] == 0)  && (FeldSpieler[3][1] == 0)  && (FeldSpieler[2][1] == 0)
				    && (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, 4, 1, 1) == false)
				    && (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, 3, 1, 1) == false))
				{
					// Grosse Rochade

					ZugTyp[Tiefe][ZM] 		= 0x30;

					ZM += 1;
				}
			}
			else				// Betrachtung fuer schwarz
			{
				if ((KRNMFS[Tiefe] == true) && (FeldSpieler[6][8] == 0)  && (FeldSpieler[7][8] == 0)
				    && (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, 6, 8, 2) == false)
				    && (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, 7, 8, 2) == false))
				{
					// Kleine Rochade

					ZugTyp[Tiefe][ZM] 		= 0x10;

					ZM += 1;
				}

				if ((GRNMFS[Tiefe] == true) && (FeldSpieler[4][8] == 0)  && (FeldSpieler[3][8] == 0) && (FeldSpieler[2][8] == 0)
				    && (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, 4, 8, 2) == false)
				    && (KoenigAngegriffen(FeldSpieler, FeldSteinTyp, 3, 8, 2) == false))
				{
					// Grosse Rochade

					ZugTyp[Tiefe][ZM] 		= 0x30;

					ZM += 1;
				}
			}
		}

		return ZM;
	}

	String ZugnameString(int[][] ZugTyp, int[][] ZugSteinTyp, int[][] ZugUmwandlungssteintyp,
			     int[][] ZugStartfeldLinie, int[][] ZugStartfeldReihe, int[][] ZugZielfeldLinie, int[][] ZugZielfeldReihe,
			     int Tiefe, int Zugnummer)
	{
		/*
			1. Bit: Schlagen? 				0 = nein, 1 = ja
			2. Bit: En passant?				0 = nein, 1 = ja
			3. Bit: Umwandlung?				0 = nein, 1 = ja
			4. Bit: Schach?					0 = nein, 1 = ja
			5. Bit: Rochade?				0 = nein, 1 = ja
			6. Bit: Falls Rochade, dann welche Groesse?	0 = klein, 1 = gross
		*/

		String ZugnameString = "";

		int TempZugTyp 			= ZugTyp[Tiefe][Zugnummer];			// Temp bedeutet "temporaer"
		int TempZugSteinTyp 		= ZugSteinTyp[Tiefe][Zugnummer];
		int TempZugUmwandlungssteintyp	= ZugUmwandlungssteintyp[Tiefe][Zugnummer];
		int TempZugStartfeldLinie	= ZugStartfeldLinie[Tiefe][Zugnummer];
		int TempZugStartfeldReihe	= ZugStartfeldReihe[Tiefe][Zugnummer];
		int TempZugZielfeldLinie	= ZugZielfeldLinie[Tiefe][Zugnummer];
		int TempZugZielfeldReihe	= ZugZielfeldReihe[Tiefe][Zugnummer];

		if ((TempZugTyp & 0x10) > 0)
		{
			if ((TempZugTyp & 0x20) > 0)		ZugnameString = "O-O-O";
			else					ZugnameString = "O-O";
		}
		else
		{
			if (TempZugSteinTyp == 1)		ZugnameString = " ";
			if (TempZugSteinTyp == 2)		ZugnameString = "S";
			if (TempZugSteinTyp == 3)		ZugnameString = "L";
			if (TempZugSteinTyp == 4)		ZugnameString = "T";
			if (TempZugSteinTyp == 5)		ZugnameString = "D";
			if (TempZugSteinTyp == 6)		ZugnameString = "K";
			if (TempZugStartfeldLinie == 1)		ZugnameString += "a";
			if (TempZugStartfeldLinie == 2)		ZugnameString += "b";
			if (TempZugStartfeldLinie == 3)		ZugnameString += "c";
			if (TempZugStartfeldLinie == 4)		ZugnameString += "d";
			if (TempZugStartfeldLinie == 5)		ZugnameString += "e";
			if (TempZugStartfeldLinie == 6)		ZugnameString += "f";
			if (TempZugStartfeldLinie == 7)		ZugnameString += "g";
			if (TempZugStartfeldLinie == 8)		ZugnameString += "h";

			ZugnameString += Integer.toString(TempZugStartfeldReihe);

			if ((TempZugTyp & 0x01) > 0)		ZugnameString += "x";
			else					ZugnameString += "-";

			if (TempZugZielfeldLinie == 1)		ZugnameString += "a";
			if (TempZugZielfeldLinie == 2)		ZugnameString += "b";
			if (TempZugZielfeldLinie == 3)		ZugnameString += "c";
			if (TempZugZielfeldLinie == 4)		ZugnameString += "d";
			if (TempZugZielfeldLinie == 5)		ZugnameString += "e";
			if (TempZugZielfeldLinie == 6)		ZugnameString += "f";
			if (TempZugZielfeldLinie == 7)		ZugnameString += "g";
			if (TempZugZielfeldLinie == 8)		ZugnameString += "h";

			ZugnameString += Integer.toString(TempZugZielfeldReihe);

			if ((TempZugTyp & 0x02) > 0)		ZugnameString += "ep";

			if ((TempZugTyp & 0x04) > 0)
			{
				if (TempZugUmwandlungssteintyp == 2)	ZugnameString += "S";
				if (TempZugUmwandlungssteintyp == 3)	ZugnameString += "L";
				if (TempZugUmwandlungssteintyp == 4)	ZugnameString += "T";
				if (TempZugUmwandlungssteintyp == 5)	ZugnameString += "D";
			}
		}

		if ((TempZugTyp & 0x08) > 0)		ZugnameString += "+";

		return ZugnameString;
	}

	void Zugauflistung()
	{
		System.out.printf("\n\nZugmoeglichkeiten:");
		System.out.printf("\n------------------\n");

		for (int i = 0; i < Zugmoeglichkeiten; i++)
		{
			System.out.printf("\n%2d.) %s", i + 1,
							ZugnameString(ZugTyp, ZugSteinTyp, ZugUmwandlungssteintyp,
								      ZugStartfeldLinie, ZugStartfeldReihe, ZugZielfeldLinie, ZugZielfeldReihe,
								      Halbzugnummer, i)
					 );
		}

		System.out.printf("\n");
	}

	void ZugAusfuehrung(int[][] FeldSpieler, int[][] FeldSteinTyp, int[][] FeldStein, int[][] SteinLinie, int[][] SteinReihe, int[][] SteinTyp,
			    int[] EnPassantLinie, boolean[] KRNMFW, boolean[] GRNMFW, boolean[] KRNMFS, boolean[] GRNMFS, boolean[] SchachGebot,
			    int[] HOBUS, int[] MZ,
			    int[][] ZugTyp,  int [][] ZugSteinTyp, int [][] ZugUmwandlungssteintyp,
			    int[][] ZugGeschlagenerSteinTyp, int[][] ZugGeschlagenerStein, int[][] ZugStein,
			    int[][] ZugStartfeldLinie, int[][] ZugStartfeldReihe, int[][] ZugZielfeldLinie, int[][] ZugZielfeldReihe,
			    int Spieler, int Tiefe, int NummerZug)
	{
		int Temp_Tiefe_Ink = Tiefe + 1;			// Temp bedeutet "temporaer", Ink  beudetet "Inkrement"
		int TempZugTyp = ZugTyp[Tiefe][NummerZug];
		int TempStein;
		int TempSteinTyp;
		int TempLinie;
		int TempReihe;
		int TempZugGeschlagenerSteinTyp;
		int TempZugGeschlagenerStein;

		EnPassantLinie[0] = 0;

		KRNMFW[Temp_Tiefe_Ink] = KRNMFW[Tiefe];
		GRNMFW[Temp_Tiefe_Ink] = GRNMFW[Tiefe];
		KRNMFS[Temp_Tiefe_Ink] = KRNMFS[Tiefe];
		GRNMFS[Temp_Tiefe_Ink] = GRNMFS[Tiefe];

		MZ[Temp_Tiefe_Ink] = MZ[Tiefe];

		if ((TempZugTyp & 0x08) > 0)	SchachGebot[0] = true;
		else				SchachGebot[0] = false;

		if ((TempZugTyp & 0x10) > 0)
		{
			HOBUS[Temp_Tiefe_Ink] = 0;

			if ((TempZugTyp & 0x20) > 0)
			{
				// Grosse Rochade

				if (Spieler == 1)
				{
					// Betrachtung fuer weiss

					KRNMFW[Temp_Tiefe_Ink] = false;
					GRNMFW[Temp_Tiefe_Ink] = false;

					SteinLinie[1][1]	= 3;
					SteinReihe[1][1]	= 1;
					FeldSpieler[5][1]	= 0;
					FeldSteinTyp[5][1]	= 0;
					FeldStein[5][1]		= 0;
					FeldSpieler[3][1]	= 1;
					FeldSteinTyp[3][1]	= 6;
					FeldStein[3][1]		= 1;

					SteinLinie[1][3]	= 4;
					SteinReihe[1][3]	= 1;
					FeldSpieler[1][1]	= 0;
					FeldSteinTyp[1][1]	= 0;
					FeldStein[1][1]		= 0;
					FeldSpieler[4][1]	= 1;
					FeldSteinTyp[4][1]	= 4;
					FeldStein[4][1]		= 3;
				}
				else
				{
					// Betrachtung fuer schwarz

					KRNMFS[Temp_Tiefe_Ink] = false;
					GRNMFS[Temp_Tiefe_Ink] = false;

					SteinLinie[2][1]	= 3;
					SteinReihe[2][1]	= 8;
					FeldSpieler[5][8]	= 0;
					FeldSteinTyp[5][8]	= 0;
					FeldStein[5][8]		= 0;
					FeldSpieler[3][8]	= 2;
					FeldSteinTyp[3][8]	= 6;
					FeldStein[3][8]		= 1;

					SteinLinie[2][3]	= 4;
					SteinReihe[2][3]	= 8;
					FeldSpieler[1][8]	= 0;
					FeldSteinTyp[1][8]	= 0;
					FeldStein[1][8]		= 0;
					FeldSpieler[4][8]	= 2;
					FeldSteinTyp[4][8]	= 4;
					FeldStein[4][8]		= 3;
				}
			}
			else
			{
				// Kleine Rochade

				if (Spieler == 1)
				{
					// Betrachtung fuer weiss

					KRNMFW[Temp_Tiefe_Ink] = false;
					GRNMFW[Temp_Tiefe_Ink] = false;

					SteinLinie[1][1]	= 7;
					SteinReihe[1][1]	= 1;
					FeldSpieler[5][1]	= 0;
					FeldSteinTyp[5][1]	= 0;
					FeldStein[5][1]		= 0;
					FeldSpieler[7][1]	= 1;
					FeldSteinTyp[7][1]	= 6;
					FeldStein[7][1]		= 1;

					SteinLinie[1][4]	= 6;
					SteinReihe[1][4]	= 1;
					FeldSpieler[8][1]	= 0;
					FeldSteinTyp[8][1]	= 0;
					FeldStein[8][1]		= 0;
					FeldSpieler[6][1]	= 1;
					FeldSteinTyp[6][1]	= 4;
					FeldStein[6][1]		= 4;
				}
				else
				{
					// Betrachtung fuer schwarz

					KRNMFS[Temp_Tiefe_Ink] = false;
					GRNMFS[Temp_Tiefe_Ink] = false;

					SteinLinie[2][1]	= 7;
					SteinReihe[2][1]	= 8;
					FeldSpieler[5][8]	= 0;
					FeldSteinTyp[5][8]	= 0;
					FeldStein[5][8]		= 0;
					FeldSpieler[7][8]	= 2;
					FeldSteinTyp[7][8]	= 6;
					FeldStein[7][8]		= 1;

					SteinLinie[2][4]	= 6;
					SteinReihe[2][4]	= 8;
					FeldSpieler[8][8]	= 0;
					FeldSteinTyp[8][8]	= 0;
					FeldStein[8][8]		= 0;
					FeldSpieler[6][8]	= 2;
					FeldSteinTyp[6][8]	= 4;
					FeldStein[6][8]		= 4;
				}
			}
		}
		else
		{
			TempStein 	= ZugStein[Tiefe][NummerZug];
			TempSteinTyp	= SteinTyp[Spieler][TempStein];
			TempLinie	= SteinLinie[Spieler][TempStein];
			TempReihe	= SteinReihe[Spieler][TempStein];

			if ((TempSteinTyp == 1) || ((TempZugTyp & 0x01) > 0))	HOBUS[Temp_Tiefe_Ink] = 0;
			else							HOBUS[Temp_Tiefe_Ink] = HOBUS[Tiefe] + 1;

			FeldSpieler[TempLinie][TempReihe]	= 0;
			FeldSteinTyp[TempLinie][TempReihe]	= 0;
			FeldStein[TempLinie][TempReihe]		= 0;

			TempLinie = ZugZielfeldLinie[Tiefe][NummerZug];
			TempReihe = ZugZielfeldReihe[Tiefe][NummerZug];

			if ((TempZugTyp & 0x01) > 0)
			{
				TempZugGeschlagenerSteinTyp	= ZugGeschlagenerSteinTyp[Tiefe][NummerZug];
				TempZugGeschlagenerStein	= ZugGeschlagenerStein[Tiefe][NummerZug];

				SteinTyp[3 - Spieler][TempZugGeschlagenerStein] = 0;

				if (Spieler == 1)
				{
					if (TempZugGeschlagenerStein == 4)	KRNMFS[Temp_Tiefe_Ink] = false;
					if (TempZugGeschlagenerStein == 3)	GRNMFS[Temp_Tiefe_Ink] = false;
				}
				else
				{
					if (TempZugGeschlagenerStein == 4)	KRNMFW[Temp_Tiefe_Ink] = false;
					if (TempZugGeschlagenerStein == 3)	GRNMFW[Temp_Tiefe_Ink] = false;
				}

				if ((TempZugGeschlagenerSteinTyp == 2) || (TempZugGeschlagenerSteinTyp == 3))	MZ[Temp_Tiefe_Ink] -= 1;
				else										MZ[Temp_Tiefe_Ink] -= 2;
			}

			if ((TempZugTyp & 0x02) > 0)
			{
				if (Spieler == 1)
				{
					FeldSpieler[TempLinie][5]	= 0;
					FeldSteinTyp[TempLinie][5]	= 0;
					FeldStein[TempLinie][5]		= 0;
				}
				else
				{
					FeldSpieler[TempLinie][4]	= 0;
					FeldSteinTyp[TempLinie][4]	= 0;
					FeldStein[TempLinie][4]		= 0;
				}
			}

			if ((TempZugTyp & 0x04) > 0)
			{
				TempSteinTyp			= ZugUmwandlungssteintyp[Tiefe][NummerZug];
				SteinTyp[Spieler][TempStein]	= TempSteinTyp;

				if ((TempSteinTyp == 2) || (TempSteinTyp == 3))	MZ[Temp_Tiefe_Ink] -= 1;
			}

			FeldSpieler[TempLinie][TempReihe]	= Spieler;
			FeldSteinTyp[TempLinie][TempReihe]	= TempSteinTyp;
			FeldStein[TempLinie][TempReihe]		= TempStein;

			SteinLinie[Spieler][TempStein] = TempLinie;
			SteinReihe[Spieler][TempStein] = TempReihe;

			if (Spieler == 1)
			{
				if ((TempStein == 1) || (TempStein == 4))	KRNMFW[Temp_Tiefe_Ink] = false;
				if ((TempStein == 1) || (TempStein == 3))	GRNMFW[Temp_Tiefe_Ink] = false;
			}
			else
			{
				if ((TempStein == 1) || (TempStein == 4))	KRNMFS[Temp_Tiefe_Ink] = false;
				if ((TempStein == 1) || (TempStein == 3))	GRNMFS[Temp_Tiefe_Ink] = false;
			}

			if ((TempSteinTyp == 1) && (((TempReihe == 4) && (ZugStartfeldReihe[Tiefe][NummerZug] == 2))
						    ||
						    ((TempReihe == 5) && (ZugStartfeldReihe[Tiefe][NummerZug] == 7))
						   )
			   )
			{
				if (((TempLinie > 1)
				     && (FeldSpieler[TempLinie - 1][TempReihe] == 3 - Spieler) && (FeldSteinTyp[TempLinie - 1][TempReihe] == 1))
				    ||
				    ((TempLinie < 8)
				     && (FeldSpieler[TempLinie + 1][TempReihe] == 3 - Spieler) && (FeldSteinTyp[TempLinie + 1][TempReihe] == 1))
				   )
				{
					EnPassantLinie[0] = TempLinie;
				}
			}
		}
	}

	int Potenz(int Basis, int Exponent)
	{
		int Potenz = 1;

		for (int i = 1; i <= Exponent; i++)
		{
			Potenz *= Basis;
		}

		return Potenz;
	}

	boolean SSBZ(int[][] FeldSpieler, int[][] FeldSteinTyp,
		     int[] EnPassantLinie, boolean[] KRNMFW, boolean[] GRNMFW, boolean[] KRNMFS, boolean[] GRNMFS,
		     int Spieler, int Tiefe,
		     int[][] SIDN, int[] SWA)		// SSBZ  bedeutet "Stellung stand bereits zweimal"
	{
		int TempSIDN0;		// Temp bedeutet "temporaer", SIDN bedeutet "Stellungsidentifikationsnummer"
		int TempSIDN1;
		int TempSIDN2;
		int TempSIDN3;
		int TempSIDN4;
		int TempSIDN5;
		int TempSIDN6;
		int TempSIDN7;
		int TempSIDN8;

		SIDN[Tiefe][0] = 1 - Tiefe % 2 + EnPassantLinie[0] * 32;
		if (KRNMFW[Tiefe]) SIDN[Tiefe][0] +=  2;
		if (GRNMFW[Tiefe]) SIDN[Tiefe][0] +=  4;
		if (KRNMFS[Tiefe]) SIDN[Tiefe][0] +=  8;
		if (GRNMFS[Tiefe]) SIDN[Tiefe][0] += 16;

		for (int i = 1; i < 9; i++)
		{
			SIDN[Tiefe][i] = 0;

			for (int j = 1; j < 9; j++)
			{
				SIDN[Tiefe][i] += FeldSteinTyp[j][i] * Potenz(13,j-1);
				if (FeldSpieler[j][i] == 2) SIDN[Tiefe][i] += 6 * Potenz(13,j-1);
			}
		}

		SWA[Tiefe] = 1;

		TempSIDN0 = SIDN[Tiefe][0];
		TempSIDN1 = SIDN[Tiefe][1];
		TempSIDN2 = SIDN[Tiefe][2];
		TempSIDN3 = SIDN[Tiefe][3];
		TempSIDN4 = SIDN[Tiefe][4];
		TempSIDN5 = SIDN[Tiefe][5];
		TempSIDN6 = SIDN[Tiefe][6];
		TempSIDN7 = SIDN[Tiefe][7];
		TempSIDN8 = SIDN[Tiefe][8];

		for (int i = 1; i < Tiefe; i++)
		{
			if ((SIDN[i][0] == TempSIDN0) 
			    && (SIDN[i][1] == TempSIDN1)
			    && (SIDN[i][2] == TempSIDN2)
			    && (SIDN[i][3] == TempSIDN3)
			    && (SIDN[i][4] == TempSIDN4)
			    && (SIDN[i][5] == TempSIDN5)
			    && (SIDN[i][6] == TempSIDN6)
			    && (SIDN[i][7] == TempSIDN7)
			    && (SIDN[i][8] == TempSIDN8)
			   )
			{
				SWA[Tiefe] = -i;
				if (SWA[i] == 2)
				{
					return true;
				}
				SWA[i] += 1;
				break;
			}
		}

		return false;
	}

	void Computerzug()
	{
		System.out.printf("\nDer Computer berechnet einen Zug.\n");

		int ComputerSpieler = SpielerAmZug(Halbzugnummer);

		// bf bedeutet "brute force"

		int[][] bf_FeldSpieler;	 // bf_FeldSpieler[Linie][Reihe]; 0 = Leeres Feld, 1 = Spieler1, 2 = Spieler2
		int[][] bf_FeldSteinTyp; // bf_FeldSteinTyp[Linie][Reihe]; 1 = Bauer, 2 = Springer, 3 = Laeufer, 4 = Turm, 5 = Dame, 6 = Koenig
		int[][] bf_FeldStein;	 // bf_FeldStein[Linie][Reihe]; SteinNummer
		int[][] bf_SteinLinie;	 // bf_SteinLinie[Spieler][SteinNummer]
		int[][] bf_SteinReihe;	 // bf_SteinReihe[Spieler][SteinNummer]
		int[][] bf_SteinTyp;	 // bf_SteinTyp[Spieler][SteinNummer]; 1 = Bauer, 2 = Springer, 3 = Laeufer, 4 = Turm, 5 = Dame, 6 = Koenig
		int[] bf_EnPassantLinie = {0};
		boolean[] bf_KRNMFW = new boolean[MaximaleZugAnzahl + 1];	// KRMFW bedeutet "Kleine Rochade noch moeglich fuer weiss"
		boolean[] bf_GRNMFW = new boolean[MaximaleZugAnzahl + 1];	// GRMFW bedeutet "Grosse Rochade noch moeglich fuer weiss"
		boolean[] bf_KRNMFS = new boolean[MaximaleZugAnzahl + 1];	// KRMFS bedeutet "Kleine Rochade noch moeglich fuer schwarz"
		boolean[] bf_GRNMFS = new boolean[MaximaleZugAnzahl + 1];	// GRMFS bedeutet "Grosse Rochade noch moeglich fuer schwarz"

		int[] bf_MZ = new int[MaximaleZugAnzahl + 1];			// MZ    bedeutet "Materialziffer"
		int[] bf_HOBUS = new int[MaximaleZugAnzahl + 1];		// HOBUS bedeutet "Halbzuege ohne Bauernbewegung und Schlagen"
		int[][] bf_SIDN = new int[MaximaleZugAnzahl + 1][9];		// SIDN  bedeutet "Stellungsidentifikationsnummer"
		int[] bf_SWA = new int[MaximaleZugAnzahl + 1];			// SWA   bedeutet "Stellungswiederholungsanzahl"

		int[][] bf_ZugTyp			= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
		int[][] bf_ZugStein			= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
		int[][] bf_ZugUmwandlungssteintyp	= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
		int[][] bf_ZugGeschlagenerSteinTyp	= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
		int[][] bf_ZugGeschlagenerStein		= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
		int[][] bf_ZugSteinTyp			= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
		int[][] bf_ZugStartfeldLinie		= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
		int[][] bf_ZugStartfeldReihe		= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
		int[][] bf_ZugZielfeldLinie		= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];
		int[][] bf_ZugZielfeldReihe		= new int[MaximaleZugAnzahl + 1][MaximaleZugmoeglichkeitenAnzahl];

		boolean[] bf_SchachGebot = {false};

		bf_FeldSpieler	= new int[9][9];
		bf_FeldSteinTyp	= new int[9][9];
		bf_FeldStein	= new int[9][9];
		bf_SteinLinie	= new int[3][17];
		bf_SteinReihe	= new int[3][17];
		bf_SteinTyp	= new int[3][17];

		for (int i = 1; i < 9; i++)
		for (int j = 1; j < 9; j++)
		{
			bf_FeldSpieler[i][j]	= FeldSpieler[i][j];			// Aus den Originalarrays kopieren
			bf_FeldSteinTyp[i][j]	= FeldSteinTyp[i][j];
			bf_FeldStein[i][j]	= FeldStein[i][j];
		}

		for (int i = 1; i <  3; i++)
		for (int j = 1; j < 17; j++)
		{
			bf_SteinLinie[i][j]	= SteinLinie[i][j];			// Aus den Originalarrays kopieren
			bf_SteinReihe[i][j]	= SteinReihe[i][j];
			bf_SteinTyp[i][j]	= SteinTyp[i][j];
		}

		bf_EnPassantLinie[0] = EnPassantLinie[0];

		for (int i = 1; i <= Halbzugnummer; i++)
		{
			bf_KRNMFW[i] = KRNMFW[i];
			bf_GRNMFW[i] = GRNMFW[i];
			bf_KRNMFS[i] = KRNMFS[i];
			bf_GRNMFS[i] = GRNMFS[i];
			bf_MZ[i] = MZ[i];
		}

		for (int i = 1; i <= Halbzugnummer; i++)
		{
			bf_HOBUS[i] = HOBUS[i];
			for (int j = 0; j < 9; j++)
			{
				bf_SIDN[i][j] = SIDN[i][j];
			}
			bf_SWA[i] = SWA[i];
		}
		for (int i = 1; i <= Halbzugnummer; i++)
		for (int j = 0; j < 9; j++)
		{
			bf_SIDN[i][j] = SIDN[i][j];
		}
		for (int i = 1; i <= Halbzugnummer; i++)
		{
			bf_SWA[i] = SWA[i];
		}

		int[] bf_ZM = new int[MaximaleZugAnzahl + 1];	// brute force Zugmoeglichkeiten
		int[] bf_NTZ = new int[MaximaleZugAnzahl + 1];	// brute force Naechster tieferer Zug
		int[] bf_ETB = new int[MaximaleZugAnzahl + 1];	// brute force Extreme tiefere Bewertung
		int[] bf_MD = new int[MaximaleZugAnzahl + 1];	// brute force Materialdifferenz

		int bf_HN = Halbzugnummer;						// brute force Halbzugnummer

		int BZB = -1000;							// brute force Bester Zug Bewertung
		int BZN = -1000;							// brute force Bester Zug Nummer

/* Dev */	int plyCounter = 0;

		bf_ZM[bf_HN] = Zugmoeglichkeiten(bf_FeldSpieler, bf_FeldSteinTyp, bf_FeldStein, bf_SteinLinie, bf_SteinReihe, bf_SteinTyp,
						 bf_EnPassantLinie, bf_KRNMFW, bf_GRNMFW, bf_KRNMFS, bf_GRNMFS,
						 bf_ZugTyp, bf_ZugSteinTyp, bf_ZugUmwandlungssteintyp,
						 bf_ZugGeschlagenerSteinTyp, bf_ZugGeschlagenerStein, bf_ZugStein,
						 bf_ZugStartfeldLinie, bf_ZugStartfeldReihe, bf_ZugZielfeldLinie, bf_ZugZielfeldReihe,
						 SpielerAmZug(bf_HN), bf_HN);
		bf_NTZ[bf_HN] = 0;
		bf_ETB[bf_HN] = -1000;
		bf_MD[bf_HN]  = 0;
		for (int i = 1; i < 17; i++)
		{
			switch(bf_SteinTyp[1][i])
			{
				case 1:
					bf_MD[bf_HN] += 1;
					break;
				case 2:
					bf_MD[bf_HN] += 3;
					break;
				case 3:
					bf_MD[bf_HN] += 3;
					break;
				case 4:
					bf_MD[bf_HN] += 4;
					break;
				case 5:
					bf_MD[bf_HN] += 9;
			}

			switch(bf_SteinTyp[2][i])
			{
				case 1:
					bf_MD[bf_HN] -= 1;
					break;
				case 2:
					bf_MD[bf_HN] -= 3;
					break;
				case 3:
					bf_MD[bf_HN] -= 3;
					break;
				case 4:
					bf_MD[bf_HN] -= 4;
					break;
				case 5:
					bf_MD[bf_HN] -= 9;
			}
		}
		if (SpielerAmZug(Halbzugnummer) == 2)	bf_MD[bf_HN] = -bf_MD[bf_HN];

		int TempSAZ;				// Temp bedeutet "temporaer", SAZ bedeutet "Spieler am Zug"
		int TempSAHZ;				// HSAZ bedeutet "Spieler am hoeheren Zug"
		int TempZN;				// ZN   bedeutet "Zugnummer"
		int TempHZN;				// HZN  bedeutet "Hoehere Zugnummer"
		int TempSF;				// SF   bedeutet "SpielerFaktor"
		int Temp_bf_HN_Ink;			// Ink  beudetet "Inkrement"
		int Temp_bf_HN_Dek;			// Ink  beudetet "Dekrement"
		int TempZugTyp;
		int TempStein;
		int TempSteinTyp;
		int TempLinie;
		int TempReihe;
		int TempZugGeschlagenerSteinTyp;
		int TempZugGeschlagenerStein;
		int TempZugUmwandlungssteintyp;

		while (true)	// Variantenbaumbetrachtung
		{
			TempSAZ = SpielerAmZug(bf_HN);
			TempZN  = bf_NTZ[bf_HN];
			if (TempSAZ == ComputerSpieler)
			{
				TempSF = 1;
			}
			else
			{
				TempSF = -1;
			}

			if ((bf_HN - Halbzugnummer >= Rechentiefe) || (TempZN == bf_ZM[bf_HN])
			    ||
			    ((TempSF == 1) && (bf_ETB[bf_HN] == 500))
			    ||
			    ((TempSF == -1) && (bf_ETB[bf_HN] == -500))
			   )
			{
				if (bf_HN == Halbzugnummer)	break;

				Temp_bf_HN_Dek	= bf_HN - 1;
				TempSAHZ	= SpielerAmZug(Temp_bf_HN_Dek);
				TempHZN		= bf_NTZ[Temp_bf_HN_Dek];
				TempZugTyp	= bf_ZugTyp[Temp_bf_HN_Dek][TempHZN];

				if (bf_ETB[bf_HN] == -1000)	bf_ETB[bf_HN] = bf_MD[bf_HN];

				if (bf_SWA[bf_HN] < 0)		bf_SWA[-bf_SWA[bf_HN]] -= 1;		// Zug zurueck nehmen

				if ((TempZugTyp & 0x10) > 0)
				{
					if ((TempZugTyp & 0x20) > 0)
					{
						// Grosse Rochade

						if (TempSAHZ == 1)
						{
							// Betrachtung fuer weiss

							bf_SteinLinie[1][1]	= 5;
							bf_SteinReihe[1][1]	= 1;
							bf_FeldSpieler[5][1]	= 1;
							bf_FeldSteinTyp[5][1]	= 6;
							bf_FeldStein[5][1]	= 1;
							bf_FeldSpieler[3][1]	= 0;
							bf_FeldSteinTyp[3][1]	= 0;
							bf_FeldStein[3][1]	= 0;

							bf_SteinLinie[1][3]	= 1;
							bf_SteinReihe[1][3]	= 1;
							bf_FeldSpieler[1][1]	= 1;
							bf_FeldSteinTyp[1][1]	= 4;
							bf_FeldStein[1][1]	= 3;
							bf_FeldSpieler[4][1]	= 0;
							bf_FeldSteinTyp[4][1]	= 0;
							bf_FeldStein[4][1]	= 0;
						}
						else
						{
							// Betrachtung fuer schwarz

							bf_SteinLinie[2][1]	= 5;
							bf_SteinReihe[2][1]	= 8;
							bf_FeldSpieler[5][8]	= 2;
							bf_FeldSteinTyp[5][8]	= 6;
							bf_FeldStein[5][8]	= 1;
							bf_FeldSpieler[3][8]	= 0;
							bf_FeldSteinTyp[3][8]	= 0;
							bf_FeldStein[3][8]	= 0;

							bf_SteinLinie[2][3]	= 1;
							bf_SteinReihe[2][3]	= 8;
							bf_FeldSpieler[1][8]	= 2;
							bf_FeldSteinTyp[1][8]	= 4;
							bf_FeldStein[1][8]	= 3;
							bf_FeldSpieler[4][8]	= 0;
							bf_FeldSteinTyp[4][8]	= 0;
							bf_FeldStein[4][8]	= 0;
						}
					}
					else
					{
						// Kleine Rochade

						if (TempSAHZ == 1)
						{
							// Betrachtung fuer weiss

							bf_SteinLinie[1][1]	= 5;
							bf_SteinReihe[1][1]	= 1;
							bf_FeldSpieler[5][1]	= 1;
							bf_FeldSteinTyp[5][1]	= 6;
							bf_FeldStein[5][1]	= 1;
							bf_FeldSpieler[7][1]	= 0;
							bf_FeldSteinTyp[7][1]	= 0;
							bf_FeldStein[7][1]	= 0;

							bf_SteinLinie[1][4]	= 8;
							bf_SteinReihe[1][4]	= 1;
							bf_FeldSpieler[8][1]	= 1;
							bf_FeldSteinTyp[8][1]	= 4;
							bf_FeldStein[8][1]	= 4;
							bf_FeldSpieler[6][1]	= 0;
							bf_FeldSteinTyp[6][1]	= 0;
							bf_FeldStein[6][1]	= 0;
						}
						else
						{
							// Betrachtung fuer schwarz

							bf_SteinLinie[2][1]	= 5;
							bf_SteinReihe[2][1]	= 8;
							bf_FeldSpieler[5][8]	= 2;
							bf_FeldSteinTyp[5][8]	= 6;
							bf_FeldStein[5][8]	= 1;
							bf_FeldSpieler[7][8]	= 0;
							bf_FeldSteinTyp[7][8]	= 0;
							bf_FeldStein[7][8]	= 0;

							bf_SteinLinie[2][4]	= 8;
							bf_SteinReihe[2][4]	= 8;
							bf_FeldSpieler[8][8]	= 2;
							bf_FeldSteinTyp[8][8]	= 4;
							bf_FeldStein[8][8]	= 4;
							bf_FeldSpieler[6][8]	= 0;
							bf_FeldSteinTyp[6][8]	= 0;
							bf_FeldStein[6][8]	= 0;
						}
					}
				}
				else
				{
					TempStein	= bf_ZugStein[Temp_bf_HN_Dek][TempHZN];
					TempLinie	= bf_ZugStartfeldLinie[Temp_bf_HN_Dek][TempHZN];
					TempReihe	= bf_ZugStartfeldReihe[Temp_bf_HN_Dek][TempHZN];
					TempSteinTyp	= bf_ZugSteinTyp[Temp_bf_HN_Dek][TempHZN];

					bf_SteinLinie[TempSAHZ][TempStein]	= TempLinie;
					bf_SteinReihe[TempSAHZ][TempStein]	= TempReihe;
					bf_FeldSpieler[TempLinie][TempReihe]	= TempSAHZ;
					bf_FeldSteinTyp[TempLinie][TempReihe]	= TempSteinTyp;
					bf_FeldStein[TempLinie][TempReihe]	= TempStein;

					if ((TempZugTyp & 0x04) > 0)
					{
						bf_SteinTyp[TempSAHZ][TempStein]	= 1;
						bf_FeldSteinTyp[TempLinie][TempReihe]	= 1;
					}

					TempLinie = bf_ZugZielfeldLinie[Temp_bf_HN_Dek][TempHZN];
					TempReihe = bf_ZugZielfeldReihe[Temp_bf_HN_Dek][TempHZN];

					if ((TempZugTyp & 0x01) > 0)
					{
						TempZugGeschlagenerSteinTyp	= bf_ZugGeschlagenerSteinTyp[Temp_bf_HN_Dek][TempHZN];
						TempZugGeschlagenerStein	= bf_ZugGeschlagenerStein[Temp_bf_HN_Dek][TempHZN];

						if ((TempZugTyp & 0x02) > 0)
						{
							bf_FeldSpieler[TempLinie][TempReihe]	= 0;
							bf_FeldSteinTyp[TempLinie][TempReihe]	= 0;
							bf_FeldStein[TempLinie][TempReihe]	= 0;

							if (TempSAHZ == 1)
							{
								bf_FeldSpieler[TempLinie][5]	= TempSAZ;
								bf_FeldSteinTyp[TempLinie][5]	= 1;
								bf_FeldStein[TempLinie][5]	= TempZugGeschlagenerStein;
							}
							else
							{
								bf_FeldSpieler[TempLinie][4]	= TempSAZ;
								bf_FeldSteinTyp[TempLinie][4]	= 1;
								bf_FeldStein[TempLinie][4]	= TempZugGeschlagenerStein;
							}
						}
						else
						{
							bf_FeldSpieler[TempLinie][TempReihe]	= TempSAZ;
							bf_FeldSteinTyp[TempLinie][TempReihe]	= TempZugGeschlagenerSteinTyp;
							bf_FeldStein[TempLinie][TempReihe]	= TempZugGeschlagenerStein;
						}

						bf_SteinTyp[TempSAZ][TempZugGeschlagenerStein]		= TempZugGeschlagenerSteinTyp;
					}
					else
					{
						bf_FeldSpieler[TempLinie][TempReihe]	= 0;
						bf_FeldSteinTyp[TempLinie][TempReihe]	= 0;
						bf_FeldStein[TempLinie][TempReihe]	= 0;
					}
				}

				if ((((TempSF == 1) && (bf_ETB[bf_HN] < bf_ETB[Temp_bf_HN_Dek])) || (bf_ETB[Temp_bf_HN_Dek] == -1000))
				    ||
				    ((TempSF == -1) && (bf_ETB[bf_HN] > bf_ETB[Temp_bf_HN_Dek]))
				   )
				{
					bf_ETB[Temp_bf_HN_Dek] = bf_ETB[bf_HN];

					if (Temp_bf_HN_Dek == Halbzugnummer)
					{
						BZB = bf_ETB[Temp_bf_HN_Dek];
						BZN = bf_NTZ[Temp_bf_HN_Dek];
					}
				}

				bf_HN -= 1;

				bf_NTZ[bf_HN] += 1;
			}
			else
			{
				Temp_bf_HN_Ink = bf_HN + 1;

				ZugAusfuehrung(bf_FeldSpieler, bf_FeldSteinTyp, bf_FeldStein, bf_SteinLinie, bf_SteinReihe, bf_SteinTyp,
					       bf_EnPassantLinie, bf_KRNMFW, bf_GRNMFW, bf_KRNMFS, bf_GRNMFS, bf_SchachGebot, bf_HOBUS, bf_MZ,
					       bf_ZugTyp, bf_ZugSteinTyp, bf_ZugUmwandlungssteintyp,
					       bf_ZugGeschlagenerSteinTyp, bf_ZugGeschlagenerStein, bf_ZugStein,
					       bf_ZugStartfeldLinie, bf_ZugStartfeldReihe, bf_ZugZielfeldLinie, bf_ZugZielfeldReihe,
					       TempSAZ, bf_HN, TempZN);

/* Dev */			plyCounter += 1;

				bf_ZM[Temp_bf_HN_Ink] = Zugmoeglichkeiten(bf_FeldSpieler, bf_FeldSteinTyp, bf_FeldStein,
									  bf_SteinLinie, bf_SteinReihe, bf_SteinTyp,
									  bf_EnPassantLinie, bf_KRNMFW, bf_GRNMFW, bf_KRNMFS, bf_GRNMFS,
									  bf_ZugTyp, bf_ZugSteinTyp, bf_ZugUmwandlungssteintyp,
									  bf_ZugGeschlagenerSteinTyp, bf_ZugGeschlagenerStein, bf_ZugStein,
									  bf_ZugStartfeldLinie, bf_ZugStartfeldReihe,
									  bf_ZugZielfeldLinie, bf_ZugZielfeldReihe,
									  3 - TempSAZ, Temp_bf_HN_Ink);

				if ((bf_SchachGebot[0] == true) && (bf_ZM[Temp_bf_HN_Ink] == 0))	// Siegbedingungen
				{
					bf_HN += 1;

					bf_NTZ[bf_HN] = bf_ZM[Temp_bf_HN_Ink];
					if (TempSF == 1)
					{
						bf_ETB[bf_HN] = 500;
					}
					else
					{
						bf_ETB[bf_HN] = -500;
					}

					continue;
				}

				if (((SchachGebot[0] == false) && (bf_ZM[Temp_bf_HN_Ink] == 0))		// Remisbedingungen
				    ||
				    (bf_HOBUS[Temp_bf_HN_Ink] == 100)
				    ||
				    (bf_MZ[Temp_bf_HN_Ink] == 1)
				    ||
				    (SSBZ(bf_FeldSpieler, bf_FeldSteinTyp,
					  bf_EnPassantLinie, bf_KRNMFW, bf_GRNMFW, bf_KRNMFS, bf_GRNMFS,
					  3 - TempSAZ, Temp_bf_HN_Ink,
					  bf_SIDN, bf_SWA)
				     == true)
				   )
				{
					bf_HN += 1;

					bf_NTZ[bf_HN] = bf_ZM[Temp_bf_HN_Ink];
					bf_ETB[bf_HN] = 0;

					continue;
				}

				if ((bf_ZugTyp[bf_HN][TempZN] & 0x01) > 0)
				{
					TempZugGeschlagenerSteinTyp = bf_ZugGeschlagenerSteinTyp[bf_HN][TempZN];

					switch(TempZugGeschlagenerSteinTyp)
					{
						case 1:
							bf_MD[Temp_bf_HN_Ink] = bf_MD[bf_HN] +   TempSF;
							break;
						case 2:
							bf_MD[Temp_bf_HN_Ink] = bf_MD[bf_HN] + 3*TempSF;
							break;
						case 3:
							bf_MD[Temp_bf_HN_Ink] = bf_MD[bf_HN] + 3*TempSF;
							break;
						case 4:
							bf_MD[Temp_bf_HN_Ink] = bf_MD[bf_HN] + 5*TempSF;
							break;
						case 5:
							bf_MD[Temp_bf_HN_Ink] = bf_MD[bf_HN] + 9*TempSF;
					}
				}
				else bf_MD[Temp_bf_HN_Ink] = bf_MD[bf_HN];

				if ((bf_ZugTyp[bf_HN][TempZN] & 0x04) > 0)
				{
					TempZugUmwandlungssteintyp = bf_ZugUmwandlungssteintyp[bf_HN][TempZN];

					switch(TempZugUmwandlungssteintyp)
					{
						case 2:
							bf_MD[Temp_bf_HN_Ink] += 3*TempSF;
							break;
						case 3:
							bf_MD[Temp_bf_HN_Ink] += 3*TempSF;
							break;
						case 4:
							bf_MD[Temp_bf_HN_Ink] += 5*TempSF;
							break;
						case 5:
							bf_MD[Temp_bf_HN_Ink] += 9*TempSF;
					}
				}

				bf_HN += 1;

				bf_NTZ[bf_HN] = 0;
				bf_ETB[bf_HN] = -1000;
			}
		}

		NummerZug = BZN;	// Den vom Computer am besten bewerteten Zug uebernehmen

/* Dev */	System.out.printf("\nRechentiefe : %d.\n", Rechentiefe);

/* Dev */	System.out.printf("\nAusgefuehrte Test-Halbzuege (Plies) : %d.\n", plyCounter);

/* Dev */	System.out.printf("\nBewertung der Stellung aus Sicht des Computers am Zuge : %d.\n", BZB);

/* Fun */	if (Halbzugnummer != 1)
/* Fun */	{
/* Fun */		System.out.printf("\n%s : \"%s\"\n", NameSpieler[SpielerAmZug(Halbzugnummer)], humanizer.provideCritiqueTaunt((float) BZB));
/* Fun */	}

		System.out.printf("\nDer Computer entscheidet sich fuer den Zug %s.\n",
				  ZugnameString(bf_ZugTyp, bf_ZugSteinTyp, bf_ZugUmwandlungssteintyp,
						bf_ZugStartfeldLinie, bf_ZugStartfeldReihe, bf_ZugZielfeldLinie, bf_ZugZielfeldReihe,
						Halbzugnummer, NummerZug)
				 );

/* Fun */	System.out.printf("\n%s : \"%s\"\n", NameSpieler[SpielerAmZug(Halbzugnummer)], humanizer.provideAnnouncementTaunt((float) BZB));
	}
}